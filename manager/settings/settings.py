import os
from uuid import uuid4
from functools import lru_cache
from pathlib import Path
from pydantic import BaseSettings, BaseModel
from typing import Optional

__version__ = 'v22.09.0'

base_path = Path(__file__).parent.parent.absolute()


class API(BaseModel):
    call_register: str
    call_unregister: str
    call_checkout: str
    call_submit: str


class Settings(BaseSettings):
    server_host: str = 'localhost'
    server_port: int = 8080
    # must be writable an contain sequence parser
    bin_path: Path = Path(base_path, 'tools', 'bin')
    parser_path: Path = Path(base_path, 'tools', 'bin', 'switching-seq-parser')
    unique_worker_name: str = str(uuid4())
    api: Optional[API]
    wait: int = 3

    class Config:
        env_prefix = 'casey_'


@lru_cache
def get_settings():
    s = Settings()
    assert s.bin_path.exists()
    assert s.bin_path.is_dir()
    assert os.access(s.bin_path, os.W_OK)

    assert s.parser_path.exists()
    assert os.access(s.parser_path, os.X_OK)

    url = f'http://{s.server_host}:{s.server_port}/' + '{0}'
    api = API(
        call_register=url.format('api/register'),
        call_unregister=url.format('api/unregister'),
        call_checkout=url.format('api/checkout'),
        call_submit=url.format('api/submit'),
    )
    s.api = api

    return s
