# About

Node workers checkout subtrees of states, perform actual computation for each
state, and submit those results to the server. Each node is independent from
other nodes and communicate only with assigned server.
