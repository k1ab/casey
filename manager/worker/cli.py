import typer

from manager.worker.worker import factory
from manager.settings import get_settings

settings = get_settings()

app = typer.Typer()


@app.command()
def run(port: str = settings.server_port):
    """Run Casey node locally"""
    settings.server_port = port

    loop, protocol = factory()
    protocol.configure(settings=settings)
    loop.start()


if __name__ == "__main__":
    app()
