import atexit
import requests

from enum import Enum, auto, unique
from time import sleep
from typing import Dict, Optional, List
from loguru import logger

from manager.tools import schemas
from manager.tools.verify import verify
from manager.settings import (
    get_settings,
    API,
)


class ProtocolInterface:
    worker: schemas.Worker
    interpreter: Optional[schemas.Interpreter] = None

    # Store computed graph nodes in memory
    work_cache: Dict[schemas.Hash, schemas.WorkResult] = {}
    task: Optional[schemas.Task] = None
    task_hashes: Optional[List[schemas.Hash]] = None

    computed: Optional[List[schemas.WorkResult]] = None

    wait_time: Optional[int] = None
    pause_time: Optional[int] = None
    stop_reason: str

    def configure(self) -> None:
        pass

    def on_start(self) -> None:
        ...

    def call_register(self) -> schemas.RegisterResponse:
        ...

    def call_unregister(self) -> None:
        ...

    def call_checkout(self) -> schemas.CheckoutResponse:
        ...

    def do_work(self) -> schemas.WorkBatch:
        ...

    def call_submit(self) -> schemas.SubmitResponse:
        ...

    def cleanup_on_shutdown(self) -> None:
        if self.interpreter:
            self.call_unregister()


@unique
class States(Enum):
    start = auto()
    stopped = auto()
    register = auto()
    checkout = auto()
    wait = auto()
    pause = auto()
    work = auto()
    submit = auto()


class WorkerLoop:
    """
    Manages state transitions and actions in each state.
    """
    protocol: ProtocolInterface
    state: States

    def __init__(self, protocol) -> None:
        self.protocol = protocol
        self.state = States.start

    def start(self):
        while self.state != States.stopped:
            call_on_enter = getattr(self, 'on_' + self.state.name)
            call_on_enter()

    def exit(self, stop_reason: str):
        logger.info("Shutting down worker loop.")
        self.protocol.cleanup_on_shutdown()

        logger.info(stop_reason)
        self.change_state_to(States.stopped)

    def change_state_to(self, new_state: States):
        self.state = new_state

    def on_start(self):
        logger.info("Started worker loop.")
        self.protocol.on_start()

        self.change_state_to(States.register)

    def on_register(self):
        logger.debug("Registering")
        response = self.protocol.call_register()

        if wait_time := response.wait:
            self.protocol.wait_time = wait_time
            self.change_state_to(States.wait)
        elif interpreter := response.interpreter:
            self.protocol.interpreter = interpreter
            self.change_state_to(States.checkout)
        elif stop := response.stop:
            self.protocol.stop_reason = stop
            self.exit(stop_reason=stop)
        else:
            raise RuntimeError(f"Unexpected call_register response {repr(response)}")

    def on_checkout(self):
        logger.debug("Checking out work")
        response = self.protocol.call_checkout()

        if response.restart:
            self.protocol.interpreter = None
            self.change_state_to(States.register)
        elif pause_time := response.pause:
            self.protocol.pause_time = pause_time
            self.change_state_to(States.pause)
        elif task := response.task:
            self.protocol.task = task
            self.change_state_to(States.work)
        else:
            raise RuntimeError(f"Unexpected checkout response {repr(response)}")

    def on_wait(self):
        logger.debug("Waiting to register")
        sleep(self.protocol.wait_time)

        self.protocol.wait_time = None
        self.change_state_to(States.register)

    def on_paused(self):
        logger.debug("Waiting to checkout work")
        sleep(self.protocol.pause_time)

        self.protocol.pause_time = None
        self.change_state_to(States.checkout)

    def on_work(self):
        logger.debug('Working hard!')
        work_batch = self.protocol.do_work()

        task_hashes = []
        for item in work_batch.results:
            self.protocol.work_cache[item.hash] = item
            task_hashes.append(item.hash)

        self.protocol.task_hashes = task_hashes
        self.protocol.computed = []
        self.change_state_to(States.submit)

    def on_submit(self):
        logger.debug('Submitting work')
        response = self.protocol.call_submit()

        if stop := response.stop:
            self.exit(stop)
        elif not response.hashes:
            logger.debug("Server didn't ask for additional info.")

            # Prepare for another task
            self.protocol.task = None
            self.protocol.task_hashes = None
            self.protocol.computed = None

            self.change_state_to(States.checkout)
        elif lookup := response.hashes:
            len_lookup = len(lookup)
            cache_size = len(self.protocol.work_cache)

            logger.debug(f"Server asked for details about {len_lookup} items.")
            items = [self.protocol.work_cache.get(key) for key in lookup]
            # Server might ask us about something we did not compute.
            # Right now with just ignore it.
            found = [i for i in items if i is not None]
            if not found:
                logger.debug(
                    f'Server asked for {len_lookup}. '
                    f'Found nothing! Cache size: {cache_size}'
                )
                self.change_state_to(States.checkout)

            self.protocol.computed = found
            self.protocol.task_hashes = []
            # Stay in the same state
            self.change_state_to(States.submit)
        else:
            raise RuntimeError(
                f"Unexpected submit response {repr(response)}"
            )


class RequestsProtocol(ProtocolInterface):
    worker: schemas.Worker
    api: API
    client: requests.Session

    def configure(
        self,
        settings=get_settings(),
        session=requests.Session(),
    ):
        self.worker = schemas.Worker(
            uid=settings.unique_worker_name
        )
        self.api = settings.api
        self.client = session
        self.client.headers['worker-uid'] = self.worker.uid

    def on_start(self):
        assert self.worker
        assert self.client
        assert self.api

        logger.info(f"Worker uid {self.worker.uid}.")

    def call_register(self) -> schemas.RegisterResponse:
        response = self.client.get(self.api.call_register)
        if not response:
            response.raise_for_status()

        data = schemas.RegisterResponse(
            **response.json(),
        )
        return data

    def call_unregister(self) -> None:
        response = self.client.get(self.api.call_unregister)
        if not response:
            response.raise_for_status()

    def call_checkout(self) -> schemas.CheckoutResponse:
        response = self.client.get(self.api.call_checkout)
        if not response:
            response.raise_for_status()

        data = schemas.CheckoutResponse(
            **response.json(),
        )
        return data

    def do_work(self) -> schemas.WorkBatch:
        assert self.task
        assert self.interpreter

        logger.debug(f'Sequences: {self.task.sequences!r}')

        results = verify(
            input_kore=self.task.kore,
            interpreter=self.interpreter,
            sequences=self.task.sequences,
        )

        return results

    def call_submit(self) -> schemas.SubmitResponse:
        assert self.task_hashes is not None
        assert self.computed is not None

        request = schemas.SubmitRequest(
            hashes=self.task_hashes,
            computed=self.computed,
        )

        response = self.client.post(
            self.api.call_submit,
            data=request.json(),
            headers={"Content-Type": "application/json"}
        )
        if not response:
            response.raise_for_status()

        data = schemas.SubmitResponse(
            **response.json(),
        )
        return data


def factory(protocol=RequestsProtocol, cleanup=True):
    instance = protocol()
    if cleanup:
        atexit.register(instance.cleanup_on_shutdown)
    return WorkerLoop(instance), instance
