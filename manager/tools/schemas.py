import base64
from functools import lru_cache
import hashlib
from typing import List, Optional, Any, Tuple
from pydantic import BaseModel, Field


@lru_cache
def hashing_algo(blob: bytes):
    return hashlib.sha256(blob).hexdigest()


class Worker(BaseModel):
    uid: str


class Interpreter(BaseModel):
    encoded: str = Field(
        ...,
        description="Encoded interpreter value"
    )

    @classmethod
    def from_bytes(cls, blob: bytes):
        encoded = base64.b64encode(blob).decode("ascii")
        return cls(encoded=encoded)

    @property
    def binary(self) -> bytes:
        return base64.b64decode(self.encoded)

    @property
    def hash(self) -> str:
        return hashing_algo(self.binary)


class RegisterResponse(BaseModel):
    wait: Optional[int] = Field(
        None,
        description="Wait time in seconds.",
    )
    interpreter: Optional[Interpreter] = Field(
        None,
        description="Interpreter body.",
    )
    stop: Optional[str] = Field(
        None,
        description="Reason for stopping the node."
    )


class Task(BaseModel):
    kore: str = Field(
        ...,
        description="Input kore",
    )
    sequences: List[Tuple[str, ...]] = Field(
        ...,
        description="List of sequences",
    )


class Hash(BaseModel):
    val: str = Field(
        ...,
        description="Hash value"
    )

    @classmethod
    def from_bytes(cls, blob: bytes):
        return cls(val=hashing_algo(blob))

    @classmethod
    def from_str(cls, text: str):
        return cls.from_bytes(text.encode('utf8'))

    def __eq__(self, other: Any) -> bool:
        if value := getattr(other, "val", None):
            other = value

        return self.val == other

    def __str__(self) -> str:
        return self.val

    def __hash__(self) -> int:
        return hash(self.val)


class CheckoutResponse(BaseModel):
    restart: bool = Field(
        False,
        description="Get new interpreter",
    )
    pause: Optional[int] = Field(
        None,
        description="Pause in seconds before retrying checkout",
    )
    task: Optional[Task] = Field(
        None,
        description="Task that needs to be processed"
    )


class WorkMeta(BaseModel):
    worker: Worker


class WorkResult(BaseModel):
    hash: Hash = Field(
        ...,
        description="Hash of the resulting kore",
    )
    original_hash: Hash = Field(
        ...,
        description="Hash of the input kore",
    )
    sequence: Tuple[str, ...] = Field(
        ...,
        description="Applied sequence",
    )
    kore: str = Field(
        ...,
        description="Result of the work",
    )
    formula_kore: str = Field(
        ...,
        description="Resulting formula in kore format",
    )
    formula_status: str = Field(
        ...,
        description="Interpreted formula status based on formula cell"
    )
    meta: Optional[WorkMeta] = Field(
        None,
        description="Info about the worker that did the work.",
    )


class WorkBatch(BaseModel):
    results: List[WorkResult] = Field(
        ...,
        description="List of processed work."
    )


class SubmitRequest(BaseModel):
    hashes: List[Hash] = Field(
        [],
        description="List of hashes computed by the worker"
    )
    computed: List[WorkResult] = Field(
        [],
        description="List of kores requested by server in SubmitResponse"
    )


class SubmitResponse(BaseModel):
    stop: Optional[str] = Field(
        None,
        description="Reason for stopping the node."
    )
    hashes: List[Hash] = Field(
        [],
        description="List of hashes, for which server needs kores"
    )
