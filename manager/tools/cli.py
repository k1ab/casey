import os
import base64
import typer
from pathlib import Path
from typing import List

from manager import tools


app = typer.Typer()


@app.command()
def build(pgm: Path, formula: Path, build_dir: Path = Path(os.getcwd())):
    """Compile a program together with a formula for further verification steps."""
    tools.build(pgm.absolute(), formula.absolute(), build_dir.absolute())


@app.command()
def verify(sequence: List[str], build_dir: Path = Path(os.getcwd())):
    """
    Verify built in formula for a given interrupt sequence. The result can be
    True, False, or Incomplete.
    """
    # TODO: validate build dir path
    # TODO: validate sequence names
    interpreter_path = Path(build_dir, "main-kompiled/interpreter")
    kore_path = Path(build_dir, "main-kompiled/initial.kore")

    with open(interpreter_path, 'rb') as f:
        interpreter = f.read()

    with open(kore_path, 'r') as fd:
        kore = fd.read()

    encoded_interpreter = base64.b64encode(interpreter).decode('ascii')

    batch = tools.verify(
        input_kore=kore,
        encoded_interpreter=encoded_interpreter,
        sequences=[sequence],
    )
    print(batch.results[0].formula_status)
