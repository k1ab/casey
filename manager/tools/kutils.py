import os
import tempfile
import subprocess
import shutil
from tempfile import TemporaryDirectory
from time import time
from pathlib import Path
from collections import namedtuple
from string import Template
from loguru import logger

from typing import List


ParserSource = namedtuple('ParserSource', ['path', 'syntax', 'main'])
ConfigVar = namedtuple('ConfigVar', ['name', 'sort', 'value'])
SeqConf = namedtuple('SeqConf', ['parser', 'template', 'interpreter'])

top_cell = Template("LblinitGeneratedTopCell{}($body)")


inj_template = Template("""Lbl'Unds'Map'Unds'{}(
    $previous_inj,
    Lbl'UndsPipe'-'-GT-Unds'{}(
        inj{SortKConfigVar{}, SortKItem{}}(\\dv{SortKConfigVar{}}("$name")),
        inj{Sort$sort{}, SortKItem{}}($value)
    ))
""")


class Timer:
    starts: dict = {}
    clicks: dict = {}

    def start(self, name: str):
        self.starts[name] = time()

    def click(self, name: str, prefix=''):
        if prefix:
            check_time = name + prefix
        else:
            check_time = name
        self.clicks[check_time] = time() - self.starts[name]

    def print_table(self):
        for name, value in self.clicks.items():
            print(f'Timer {name}: {value}')


def inj_sort(configs: List[ConfigVar]):
    previous_inj = "Lbl'Stop'Map{}()"
    for name, sort, value in configs:
        previous_inj = inj_template.substitute(
            previous_inj=previous_inj,
            name=name,
            sort=sort,
            value=value,
        )

    return top_cell.substitute(body=previous_inj)


def run(*options):
    result = subprocess.run(
        options,
        capture_output=True,
    )

    stderr = result.stderr.decode()

    if result.returncode != 0:
        raise RuntimeError(stderr)

    # There may be some useful warnings that we will want to fix
    if stderr:
        # join() fails if one of the arguments is PosixPath
        options = ' '.join([str(o) for o in options])
        print(f"{options}\n{stderr}", end="")

    return result.stdout.decode()


def kompile(target: Path, *options):
    return run('kompile', target, *options)


def kparse(target: Path, *options):
    return run('kparse', target, *options)


def build_parsers(copy_to: str, sources: list):
    with tempfile.TemporaryDirectory() as tmpdir:
        logger.info(f'Temp dir: {tmpdir}')
        for parser in sources:
            logger.info(f'Compiling parser: {parser.path}')
            kompile(
                parser.path,
                '--gen-bison-parser',
                '--main-module', parser.main,
                '--syntax-module', parser.syntax,
                '-d', tmpdir,
            )
            shutil.copy(
                Path(tmpdir, f'{Path(parser.path).stem}-kompiled', 'parser_PGM'),
                Path(copy_to, f'{Path(parser.path).stem}-parser'),
            )


def write(path: Path, chunk: str) -> None:
    with open(path, 'wb') as f:
        f.write(chunk.encode())
        f.flush()


def collect(processes: List[subprocess.Popen]) -> List[str]:
    result = []
    for p in processes:
        out, _ = p.communicate()
        result.append(out.decode())
    return result


def exec_batch(path: str, data: List[str], *args) -> List[str]:
    proc_list = []

    dir = "/dev/shm"

    # Workaround for non-linux systems
    if not os.path.exists(dir):
        dir = "/tmp"

    with TemporaryDirectory(dir=dir) as tmpd:
        for num, chunk in enumerate(data):
            name = Path(tmpd, str(num))
            write(name, chunk)
            proc_list.append(
                subprocess.Popen(
                    [path, name, *args],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                )
            )
        result = collect(proc_list)

    return result


def find_rparen(text: str) -> int:
    paren_num = 0
    pos = 0
    for pos, char in enumerate(text):
        if char == ')' and paren_num == 0:
            break
        if char == ')':
            paren_num -= 1
        if char == '(':
            paren_num += 1
    return pos


def lbl_split(source: str, lbl_name: str):
    """
    Find provided label name in the kore source.
    Return everything before the body inside (),
    the body itself, and the tail which is the rparen ) itself
    and everything after it.
    """
    lbl_name = f"Lbl'-LT-'{lbl_name}'-GT-'"
    seq_start = source.find(lbl_name)
    seq_start += source[seq_start:].find('(') + 1

    head, tail = source[:seq_start], source[seq_start:]

    seq_end = find_rparen(tail)
    body, tail = tail[:seq_end], tail[seq_end:]

    return head, body, tail


def lbl_replace(source, lbl_name, text):
    head, _, tail = lbl_split(source, lbl_name)
    return head + text + tail


def get_formula_cell(kore: str) -> str:
    _, formula, _ = lbl_split(
        kore, "formula",
    )
    return formula


def interpret_formula(formula_kor: str):
    true = "inj{SortLTLResult{}, SortLTLFormula{}}(LblTrue'Unds'LTL-SYNTAX'Unds'LTLResult{}())"
    false = "inj{SortLTLResult{}, SortLTLFormula{}}(LblFalse'Unds'LTL-SYNTAX'Unds'LTLResult{}())"
    if formula_kor == true:
        return "True"
    if formula_kor == false:
        return "False"

    return "Incomplete"
