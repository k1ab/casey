from pathlib import Path
from string import Template
from loguru import logger

from typing import Tuple

from .kutils import (
    kompile,
    kparse,
    build_parsers,
    inj_sort,
    ConfigVar,
    ParserSource,
    exec_batch,
)

from manager.settings import base_path

from . import schemas


def build(
    pgm: Path,
    formula: Path,
    build_dir: Path = base_path.parent,
) -> Tuple[schemas.Interpreter, str]:

    target = Path(base_path.parent, 'ltl/main.k')
    seq_parser = Path(base_path.parent, 'ltl/switching-seq.k')

    build_target = Path(build_dir, f'{target.stem}-kompiled')

    logger.info(f'Building: {target}')
    logger.info(f'Into build dir {build_dir}')
    kompile(target, '--emit-json', '-d', str(build_dir))

    logger.info('Building parsers.')
    build_parsers(
        copy_to=str(build_target),
        sources=[
            ParserSource(seq_parser, 'PARSER', 'PARSER'),
            # ParserSource('ltl/formula.k', 'FORMULA', 'FORMULA'),
            # ParserSource('c--/common-config.k', 'CONFIG-SYNTAX', 'CONFIG'),
        ]
    )

    logger.info('Generating sequence template.')
    # preparse components that will not change for every execution
    parsed_formula = kparse(formula, '--sort', 'LTLFormula', '-d', str(build_dir))
    parsed_pgm = kparse(pgm, '-d', str(build_dir))

    # create a template with $seq_value placeholder.
    seq_template = inj_sort([
        ConfigVar('$SEQ', 'SwitchingSequence', '$seq_value'),
        ConfigVar('$LTL', 'LTLFormula', parsed_formula),
        ConfigVar('$PGM', 'TranslationUnit', parsed_pgm),
    ])
    with open(Path(build_target, 'seq.template'), 'w', encoding='utf8') as f:
        f.write(seq_template)

    template = Template(seq_template)
    empty_list = "inj{SortList{}, SortSwitchingSequence{}}(Lbl'Stop'List{}())"
    initial_kore_input = template.safe_substitute(seq_value=empty_list)

    logger.info('Running interpreter with empty sequence.')
    interpreter_path = Path(build_target, 'interpreter')
    kore = exec_batch(
        str(interpreter_path),
        [initial_kore_input],
        '-1',
    )[0]
    with open(Path(build_target, 'initial.kore'), 'w', encoding='utf8') as f:
        f.write(kore)

    with open(interpreter_path, 'rb') as f:
        interpreter = schemas.Interpreter.from_bytes(f.read())

    logger.info('Done!')

    return interpreter, kore


if __name__ == '__main__':
    build(
        Path(base_path, 'ltl/tests/fixtures/peterson.cmm'),
        Path(base_path, 'ltl/tests/fixtures/formula.input'),
    )
