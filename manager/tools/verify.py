#!/usr/bin/env python3

import os
from pathlib import Path
from typing import List, Tuple

from manager.settings import (
    get_settings,
    Settings,
)

from . import schemas
from .kutils import (
    exec_batch,
    lbl_replace,
    interpret_formula,
    get_formula_cell,
)


def convert_to_k_list(items: Tuple[str, ...]) -> str:
    return ' '.join([f'ListItem({i})' for i in items])


def verify(
    input_kore: str,
    interpreter: schemas.Interpreter,
    sequences: List[Tuple[str, ...]],
    settings: Settings = get_settings()
) -> schemas.WorkBatch:
    """
    Takes initial kore file, interpreter, and sequences.
    Runs resulting kores through the interpreter.
    Assembles the result.
    """
    interpreter_path = Path(settings.bin_path, 'interpreter')

    # Create a new executable interpreter file
    if interpreter_path.exists():
        os.remove(interpreter_path)
    with open(interpreter_path, 'wb') as f:
        f.write(interpreter.binary)
    os.chmod(interpreter_path, 0o744)

    # Using built-in sequence parser convert python lists into sequences
    k_strings = [convert_to_k_list(i) for i in sequences]
    parsed_sequences = exec_batch(str(settings.parser_path), k_strings)

    # Generate multiple different kores by inserting one sequence at a time
    # into the original kore
    kores = [lbl_replace(input_kore, "switchingSeq", s) for s in parsed_sequences]

    # In parallel run all those kores through interpreter
    # and gather the resulting kore files.
    new_kores = exec_batch(str(interpreter_path), kores, '-1')

    # Delete interpreter
    interpreter_path.unlink()

    # Assemble the result
    results = []
    for k, s in zip(new_kores, sequences):
        formula = get_formula_cell(k)
        results.append(
            schemas.WorkResult(
                hash=schemas.Hash.from_str(k),
                original_hash=schemas.Hash.from_str(input_kore),
                kore=k,
                sequence=s,
                formula_kore=formula,
                formula_status=interpret_formula(formula)
            )
        )
    return schemas.WorkBatch(results=results)


if __name__ == '__main__':
    interpreter_path = Path("main-kompiled/interpreter")
    kore_path = Path("main-kompiled/initial.kore")

    with open(interpreter_path, 'rb') as f:
        interpreter = schemas.Interpreter.from_bytes(f.read())

    with open(kore_path, 'r') as fd:
        kore = fd.read()

    sequence1 = (
        'main', 'bar', 'main', 'bar', 'main',
        'bar', 'main', 'bar', 'main', 'bar',
        'main', 'main', 'main', 'main', 'main', 'main',
    )

    # main locks first, bar skips, main executes after bar
    sequence2 = (
        'main', 'main', 'bar', 'bar', 'bar', 'bar', 'bar',
        'main', 'main', 'main', 'main', 'main', 'main', 'main',
        'main', 'main',
    )

    batch = verify(
        input_kore=kore,
        interpreter=interpreter,
        sequences=[sequence1, sequence2]
    )

    print(batch.results[0].hash)
    print(batch.results[0].sequence)
    print()
    print(batch.results[1].hash)
    print(batch.results[1].sequence)
