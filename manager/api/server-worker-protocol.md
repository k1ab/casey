---
imagine.plantuml.im_opt: "width=60%"
imagine.plantuml.im_fmt: svg
imagine.graphviz.im_fmt: svg
imagine.fdp.im_fmt: svg
---

# Communication protocol between server and workers (nodes)

## Desired properties of the protocol

- scalable
- fully asynchronous: jobs, requests and replies may go in and out in any order
- robust to connection losses, repeating data, sending outdated date, etc

Highlights:

- Scheme is pull - based, i.e. nodes pull their jobs from server
- Nodes are active agents and initiate data exchange

Short description:

- node after initialization connects to predefined (in config file, for instance) server
- server gives node its new ID
- then node starts to pull jobs from server, process them and send results

## Protocol diagram

```plantuml
@startuml
participant Node as N #LightGreen
participant Server as S #LightSalmon

activate N
N -[#DarkGreen]> S : I'm new node and ready to serve!
activate S
S -[#DarkRed]-> N : Your new name, my little padavan, is QWERTY123
...
loop
...
alt some jobs are present
  N -[#DarkGreen]> S : It is QWERTY123. Give me some job.
  S -[#DarkRed]-> N : QWERY123, here is one for you: CFG(SHA: 123ABC) + tree
else no job is prepared yet
  loop
    N -[#DarkGreen]> S : It is QWERTY123. Give me so some job.
    S -[#DarkRed]-> N : QWERTY123, please, be patient, I'll prepare some for you soon.
    N -[#DarkGrey]> N : Wait for a few seconds, and try again
  end
end
...
alt server is ready
  N -[#DarkGreen]> S : It is QWERTY123. Job for SHA: 123ABC is done.\nHere is a tree with SHA of resulting configs.
  S -[#DarkRed]-> N : QWERTY123, Give me configs for next SHA sums: ...
else server is not ready
  loop
    N -[#DarkGreen]> S : It is QWERTY123. Job for SHA: 123ABC is done.\nHere is a tree with SHA of resulting configs.
    S -[#DarkRed]-> N : QWERTY123, Try one more time later.
    N -[#DarkGrey]> N : Wait for a few seconds, and try again
  end
end
...
alt
  N -[#DarkGreen]> S : It is QWERTY123. Here is SHA sums of requested configs for Job(SHA: 123ABC): ...
  S -[#DarkRed]-> N : QWERTY123, Confirmed. You may drop all the data for Job(SHA: 123ABC)
else
  loop
    N -[#DarkGreen]> S : It is QWERTY123. Here is SHA sums of requested configs for Job(SHA: 123ABC): ...
    S -[#DarkRed]-> N : QWERTY123, try again later
    N -[#DarkGrey]> N : Wait for a few seconds, and try again
  end
end
...
end
N -[#DarkGreen]> S : It is QWERTY123. bye-bye
S -[#DarkRed]-> N : QWERTY123, bye.
deactivate S
deactivate N
@enduml
```

## Node algorithm

### Toplevel

```plantuml
@startuml
start
(A)
: Init }
(A)
if (init successful?) then (yes)
  (B)
  : Processing }
  (B)
endif
: Exit;
stop
@enduml
```

### Init

```plantuml
@startuml
(A)
group Get ID
: read config <
#LightGreen: status = success /
repeat
if (timeout?) then (yes)
  #Pink: show warning;
  #Red: status = error /
  break
endif
: connect to server >
: receive ID <
repeat while (success?) is (no) not (yes)
end group
(A)
@enduml
```

### Processing

```plantuml
@startuml
(B)
repeat
: fetch job from server
  (startSHA,
   CFG data,
   cells for projection,
   tree of transitions) <
(C)
: Compute steps }
(C)
(D)
: Send results }
(D)
repeat while
(B)
@enduml
```

#### compute steps

Main algorithm:

- compute initial list of tasks for the given initial config and initial tree(forest) of transitions:

initial tree (forest):

```graphviz
digraph {
  N0 [label="N0(0, '')"];
  N1 [label="N1(1, '')"];
  N2 [label="N2(2, '')"];
  N3 [label="N3(4, '')"];
  N4 [label="N4(2, '')"];
  N5 [label="N5(0, '')"];
  N6 [label="N6(4, '')"];

  subgraph cluster_0 {
    style=filled;
    color=lightgrey;
    pencolor=black;
    penwidth=1;
    N0 -> {N1 N2}
    N1 -> {N3}
    N4 -> {N5 N6}
  }
}
```

we have nodes `Nx(idx, sha)` and each node contains idx of the thread to switch to
and the sha sum of a config for this node. Initially sha sums are empty, we fill them
during steps calculation.

subtrees:

```graphviz
digraph {
  N0 [label="N0(0, '')"];
  N1 [label="N1(1, '')"];
  N2 [label="N2(2, '')"];
  N3 [label="N3(4, '')"];
  subgraph cluster_0 {
    style=filled;
    color=lightgreen;
    pencolor=black;
    penwidth=1;
    N0 -> {N1 N2}
    N1 -> {N3}
  }
}
```

```graphviz
digraph {
  N4 [label="N4(2, '')"];
  N5 [label="N5(0, '')"];
  N6 [label="N6(4, '')"];
  subgraph cluster_0 {
    style=filled;
    color=bisque;
    pencolor=black;
    penwidth=1;
    N4 -> {N5 N6}
  }
}
```

tasks: `(startSHA, CFGdata, ref_to(N0))`, `(startSHA, CFGdata, ref_to(N4))`

- and put these tasks into list `tasks`

- then in loop until `tasks` is not empty:

  - fetch `task`: `(SHA, taskCFG, ref_to(Nx))`
  - make `cfg`: `cfg = set_next_thread_cell(taskCFG, Nx.idx)`
  - execute llvm-interpreter on `cfg` : `result_cfg = interpret(cfg)`
  - calc its `sha`: `sha = calc_sha(filter(result_cfg, projection_cells))`
  - store it: `store(sha, result_cfg)`
  - store sha in the corresponding node: `Nx.sha = sha`
  - make new tasks for children of `Nx`: `new_tasks=[(sha, result_cfg, ref_to(n)) for n in children(Nx)]`
  - add these `new_tasks` into `tasks`: `tasks.append(new_tasks)`

```plantuml
@startuml
(C)
: tasks = [(startSHA, startCFG, ref_to_(n)) for n in top_nodes(tree)] /
: results = dict{} /
repeat
  : task = tasks.pop() /
  note right
    task.sha - SHA sum of config for node
    task.cfg - config data for node
    task.node - ref to node in tree
  end note
  note left
    node.idx - next thread id
    node.sha - sha of config for node
  end note
  : cfg = set_cell(<next_thread>, task.cfg, task.node.idx) /
  : result_cfg = interpret(cfg) /
  : projection = filter(result_cfg, projection_cells) /
  : sha = calc_sha(projection) /
  : results[sha] = result_cfg /
  : store sha sum in node
    task.node.sha = sha /
  : new_tasks = [(sha, result_cfg, ref_to(n)) for n in children(task.node)] /
  note left
    new_tasks may be
    empty if task.node
    is a leaf
  end note
  : tasks.append(new_tasks) ;
repeat while (tasks empty?) is (no) not (yes)
(C)
@enduml
```

#### send results

```plantuml
@startuml
(D)
repeat
: send to server
  (startSHA,
   tree of SHAs) >
: get server reply <
if (reply == get all) then (yes)
    : send all configs
      (startSHA,
       [(SHA, cfg data) ...] >
endif
if (reply == get specified SHA sums) then (yes)
    : send selected configs
      (startSHA,
       [(SHA, cfg data) ...] >
endif
if (reply == get ... ) then (yes)
  break
endif
if (reply == server busy) then (yes)
    #Aquamarine: wait ;
endif
repeat while
(D)
@enduml
```
