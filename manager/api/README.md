# About Server

Aggregate and analyze verification results received from worker nodes.

<!--nav-->
* [About](README.md)
* [Worker Protocol](server-worker-protocol.md)
