from pathlib import Path
from tempfile import TemporaryDirectory

from fastapi import Depends, FastAPI, Header, HTTPException, UploadFile, Request
from loguru import logger

from manager.tools import schemas
from manager.tools import build
from manager import settings

from .model import global_state
from .log import init_logging

# Access to in memory state
from . import model as _db

app = FastAPI()


def get_settings(request: Request):
    """Use settings attached to the app or load fresh settings."""
    return getattr(
        request.app,
        "settings",
        settings.get_settings(),
    )


def require_uid(worker_uid: str = Header(default=None)):
    if not worker_uid:
        msg = "No worker uid in request headers"

        logger.error(msg)
        raise HTTPException(status_code=400, detail=msg)

    return worker_uid


def require_registration(worker_uid: str = Depends(require_uid)):
    if not global_state.is_processing_worker(worker_uid):
        msg = f"Worker uid {worker_uid!r} is not registered"

        logger.error(msg)
        raise HTTPException(status_code=400, detail=msg)

    return worker_uid


@app.get(
    "/api/register",
    response_model=schemas.RegisterResponse,
)
def api_register(
    worker_uid: str = Depends(require_uid),
    settings=Depends(get_settings),
):
    response = schemas.RegisterResponse()

    # Find suitable interpreter
    if interpreter := global_state.add_worker(worker_uid):
        logger.debug(f"Node {worker_uid!r} is registered")
        response.interpreter = interpreter
    else:
        # Tell node to wait
        logger.debug(f"No available interpreter for {worker_uid!r}, wait {settings.wait} seconds")
        response.wait = settings.wait

    return response


@app.get(
    "/api/checkout",
    response_model=schemas.CheckoutResponse,
)
def api_checkout(worker_uid: str = Depends(require_registration)):
    response = schemas.CheckoutResponse()

    if task := global_state.get_next_task(worker_uid):
        logger.debug(f"New task is sent to {worker_uid!r}")
        response.task = task
    else:
        global_state.remove_worker(worker_uid)
        logger.debug(f"{worker_uid!r} should register again")
        response.restart = True

    return response


@app.post(
    "/api/submit",
    response_model=schemas.SubmitResponse,
)
def api_submit(
    results: schemas.SubmitRequest,
    worker_uid: str = Depends(require_registration)
):
    response = schemas.SubmitResponse()
    response.hashes = global_state.store_results(worker_uid, results)

    logger.debug(f"Ask {worker_uid!r} for kores {response.hashes!r}")

    return response


@app.get("/api/unregister")
def api_unregister(worker_uid: str = Header(default=None)):
    if worker_uid:
        global_state.remove_worker(worker_uid)

    return "success"


@app.post("/api/upload")
async def api_upload(file: UploadFile, formula: str):
    logger.debug(f"Building {file.filename!r} file with {formula=!r}")
    with TemporaryDirectory() as build_dir:
        pgm_file = Path(build_dir, file.filename)
        ltl_file = Path(build_dir, "formula.ltl")

        # Write input file and ltl formula to disk
        with open(pgm_file, "w", encoding="utf8") as pgm_fh:
            pgm_fh.write((await file.read()).decode("utf8"))

        with open(ltl_file, "w", encoding="utf8") as ltl_fh:
            ltl_fh.write(formula)

        # TODO: catch errors
        interpreter, kore = build(pgm_file, ltl_file, Path(build_dir))

    global_state.add_interpreter(interpreter, kore)

    return "success"


@app.get("/api/get/{field}")
def in_memory_field_access(field: str):
    if field == 'available_work':
        key = list(_db.available_work.keys())[0]
        data = _db.available_work[key]
    else:
        data = getattr(_db, field)
    return data


@app.on_event("startup")
async def startup_event():
    # Redirect uvicorn loggers
    init_logging()
