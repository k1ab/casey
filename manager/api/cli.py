import pathlib
import sys

import requests
import typer
import uvicorn

from manager.api import main
from manager.settings import get_settings

settings = get_settings()

app = typer.Typer()


@app.command()
def run(
    host: str = "0.0.0.0",
    port: int = settings.server_port,
):
    """Run Casey server"""
    settings.server_host = host
    settings.server_port = port

    # add modified settings to the app
    setattr(main.app, 'settings', settings)

    uvicorn.run(
        main.app,
        host=settings.server_host,
        port=settings.server_port,
        debug=False,
    )


@app.command()
def dev():
    """Run Casey development server with settings loaded from environment."""

    uvicorn.run(
        "casey.server.api:app",
        host=settings.server_host,
        port=settings.server_port,
        reload=True,
        debug=True,
    )


@app.command()
def upload(
    file: pathlib.Path,
    formula: str,
    host: str = settings.server_host,
    port: int = settings.server_port,
):
    """Upload .cmm file and LTL formula to the Casey server"""
    if not file.exists():
        sys.exit(f"File {str(file)!r} doesn't exist")

    with open(file, "rb") as fh:
        r = requests.post(
            f"http://{host}:{port}/api/upload",
            params={"formula": formula},
            files={"file": (file.name, fh)},
            timeout=90,
        )
        r.raise_for_status()


if __name__ == "__main__":
    app()
