from typing import Dict, Optional, Set, List
from queue import Queue
from loguru import logger
from manager.tools import schemas
from manager.api.sequences import get_sequences

# {'interpreter_hash', interpreter}
interpreters: Dict[str, schemas.Interpreter] = {}
# {'interpreter_hash', Queue[task]}
available_work: Dict[str, "List[schemas.Task]"] = {}
# {'worker_id', task}
checked_out_work: Dict[str, schemas.Task] = {}
# {'worker_id': 'interpreter_hash'}
workers: Dict[str, str] = {}

# Set of hashes for all states that are:
# - already processed by some node
# - ALL next states are either in "seen" already, or in "available_work" queue
# {'interpreter_hash', Set[kore_hash]}
seen: Dict[str, Set[schemas.Hash]] = {}


class State:
    def add_worker(self, worker_uid: str) -> Optional[schemas.Interpreter]:
        """
        Add worker to the list of processing workers.
        Each worker receives an interpreter and is able to get_next_task
        for the interpreter.
        """
        if not available_work:
            return None

        if self.is_processing_worker(worker_uid):
            # Worker can work only on one task at a time
            raise RuntimeError(f"Worker with {worker_uid} already registered.")

        if len(available_work) > 1:
            # Later we might add processing multiple interpreters but
            # for now there must be only one interpreter at a time.
            raise NotImplementedError(
                "Multiple interpreter selection is not implemented"
            )
        # Pick interpreter from available work
        interpreter_hash, _ = list(available_work.items())[0]

        # Assign interpreter to the worker
        workers[worker_uid] = interpreter_hash

        return interpreters[interpreter_hash]

    def is_processing_worker(self, worker_uid: str) -> bool:
        """
        True if worker already received an interpreter.
        """
        return worker_uid in workers

    def get_next_task(self, worker_uid: str) -> Optional[schemas.Task]:
        """
        Get next task for a specific interpreter.
        """

        if worker_uid in checked_out_work:
            # Submit results for previously checked out work first!
            return None

        interpreter_hash = workers[worker_uid]
        tasks = available_work[interpreter_hash]
        if not tasks:
            return None

        task = tasks.pop()
        checked_out_work[worker_uid] = task
        return task

    def remove_worker(self, worker_uid: str) -> None:
        """
        Remove the worker from the list of processing workers and move
        'in progress' task back to available tasks.
        """
        task = checked_out_work.get(worker_uid, None)
        if task is None:
            return

        del checked_out_work[worker_uid]
        interpreter_hash = workers.pop(worker_uid)
        available_work[interpreter_hash].insert(0, task)

    def add_interpreter(
        self,
        interpreter: schemas.Interpreter,
        initial_kore: str,
    ) -> None:
        """
        Add interpreter and start initial available work queue.
        """
        key = interpreter.hash

        already_added = key in available_work or key in interpreters
        if already_added:
            return

        # create initial work queue
        available_work[key] = [
            schemas.Task(kore=initial_kore, sequences=get_sequences(1))
        ]
        interpreters[key] = interpreter

        # Prepare seen for future use
        seen[key] = set()

    def store_results(self, worker_uid: str, results: schemas.SubmitRequest) -> List[schemas.Hash]:
        if worker_uid not in checked_out_work:
            raise RuntimeError(f"Worker {worker_uid} attempts to submit before checkout.")

        interpreter_hash = workers[worker_uid]
        assert interpreter_hash in available_work

        # Worker submits kores ONLY if explicitly asked by server
        # Use them to generate new tasks
        if results.computed:
            for item in results.computed:
                if item.hash in seen[interpreter_hash]:
                    # We already processed this hash before
                    continue

                sequences = get_sequences(1)
                logger.debug(f"Adding new sequences: {sequences!r}")

                # Create new tasks based on leaf nodes
                available_work[interpreter_hash].insert(
                    0,
                    schemas.Task(kore=item.kore, sequences=sequences)
                )
                # Mark those nodes as seen
                seen[interpreter_hash].add(item.hash)

        # Otherwise return hashes of states, for which you want to get kores
        not_seen = [h for h in results.hashes if h not in seen[interpreter_hash]]

        return not_seen


global_state = State()
