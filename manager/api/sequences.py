from functools import lru_cache
from itertools import product

from typing import List, Tuple


@lru_cache
def get_sequences(
    length: int, identifiers: List[str] = ["main", "bar"]
) -> List[Tuple[str, ...]]:
    if length < 0:
        raise RuntimeError("Sequences of negative depth are not possible")

    if length == 0:
        return []

    return list(product(identifiers, repeat=length))  # type: ignore
