# Formula parser

Based on LTL syntax K framework can generate a [bison parser](https://www.gnu.org/software/bison/).

## Requirements

```{.k .ltl-formula .requirements}
requires "ltl.md"
requires "../common/identifier-syntax.md"
```

In order to generate LTL formula parser we need LTL syntax and we must declare concrete subsort of `StateLabel`.
That is why we import LTL syntax and Identifier syntax.

```{.k .ltl-formula .module}
module FORMULA
  imports LTL-SYNTAX
  imports IDENTIFIER-SYNTAX
```

## StateLabel subsort

Declare Identifier to be a subsort of `StateLabel`:

```{.k .ltl-formula .module}

  syntax StateLabel ::= Identifier
```

## Helper configuration

This tells parser generator that we want to parse `LTLFormula`:

```{.k .ltl-formula .module}
  configuration
    <k> $PGM:LTLFormula </k>
```

```{.k .ltl-formula .module}
endmodule
```

## Generating bison parser

```bash
kompile --gen-bison-parser --main-module FORMULA --syntax-module FORMULA semantics/ltl/formula.md
```

## Testing parser

In order to run those tests from pytest run:

```example
pytest --codeblocks semantics/ltl/formula.md
```

This will generate a bison parser for the ltl formula module and execute the following:

```bash
cat > test.formula <<EOF
([] abc) && (! [] test)
EOF
formula-kompiled/parser_LTLFormula_FORMULA test.formula
```

The expected parser output:
<!--pytest-codeblocks:expected-output-->
```output
Lbl'UndsAnd-And-UndsUnds'LTL-SYNTAX'Unds'LTLFormula'Unds'LTLFormula'Unds'LTLFormula{}(Lbl'LSqBRSqBUndsUnds'LTL-SYNTAX'Unds'LTLFormula'Unds'LTLFormula{}(inj{SortId{}, SortLTLFormula{}}(\dv{SortId{}}("abc"))),Lbl'BangUndsUnds'LTL-SYNTAX'Unds'LTLFormula'Unds'LTLFormula{}(Lbl'LSqBRSqBUndsUnds'LTL-SYNTAX'Unds'LTLFormula'Unds'LTLFormula{}(inj{SortId{}, SortLTLFormula{}}(\dv{SortId{}}("test")))))
```

## Cleanup

```bash
rm -r formula-kompiled
rm *.formula
```
