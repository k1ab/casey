# LTL adapter for working with global variables

This module depends on `<globals>` configuration and two ltl specific steps.
Those steps are `#step(LTLState)` and `#step(LTLLastState)`.
Users of this module can declare those steps and the module will create labels necessary for
evaluating a new state based on variables available in `<globals>` configuration.

There are two types of states. The first one is the standard state marked as `#step(LTLState)`.
The second one is the final state marked as `#step(LTLLastState)`.
It is up to the user of the module to make sure that the standard state is never called after the final state.

## Requirements


```{.k .global-var-adapter .requirements}
requires "../common/step.md"
```
