# About

This module evaluates LTL formulas during execution/simulation or symbolic execution process.

## The goal

The goal of the section is to describe a formal way to express program properties through LTL formulas.

## Approach

We assume that programs have multiple critical areas.
Those areas can be represented as a sequence of states.
We want to make sure that certain properties hold for the sequence of states.
This can be expressed using Linear Temporal Logic (LTL) formulas.
We use K framework to define syntax and semantics of our LTL formulas.
This way we can integrate other components written in K framework.

## Section navigation

<!--nav-->
* [About](README.md)
* [LTL semantics](ltl.md)
* [Formula parser](formula.md)
* [Adapter for working with global variables](global-var-adapter.md)
* [Usage example](../../tests/semantics/ltl-example.md)
