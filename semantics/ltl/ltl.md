# LTL semantics

This module provides support for Linear Temporal Logic (LTL) formulas.

## Syntax

First we import modules that will be used in the definition:

```{.k .ltl .syntax}
module LTL-SYNTAX
```

```{.k .ltl .syntax}
  imports BOOL-SYNTAX
  imports SET
```

### StateLabel

Initially we define `StateLabel` sort that represents the conditions of a program properties.
Users of LTL module **must** define a sub sort from `StateLabel`.
The choice of a particular `StateLabel` structure is up to the user.
The most convenient way is to sub sort `Id` sort.

```{.k .ltl .syntax}
  syntax StateLabel
```

### LTLResult

LTL formula finally evaluates to either `True` or `False` so we define `LTLResult` as "True" or "False":

```{.k .ltl .syntax}
  syntax LTLResult ::= "True" | "False"
```

### LTLFormula

Now we can define our `LTLFormula` as:

```{.k .ltl .syntax}
  syntax LTLFormula ::= LTLResult
                      | StateLabel
                      | "(" LTLFormula ")" [bracket]
```

#### Boolean operators

Those operators are _not_, _or_, _and_, _implies_, _xor_:

```{.k .ltl .syntax}
                      | "!" LTLFormula
                      | LTLFormula "||" LTLFormula [right]
                      | LTLFormula "&&" LTLFormula [right]
                      | LTLFormula "->" LTLFormula [right]
                      | LTLFormula "^"  LTLFormula [right]
```

#### _Always_ []

```{.k .ltl .syntax}
                      | "[]" LTLFormula
```

#### _Eventually_ <>

```{.k .ltl .syntax}
                      | "<>" LTLFormula
```

#### _Multiple_ formulas `;`

Provide a way to specify one formula for a specific state.
For example in the current state first formula should hold, in the next state the second one should hold.
This might be useful to check for initial conditions first.
For example in the first state we want:

```example
a && b; []c
```

In this example `c` must always be `True` except in the initial state.

```{.k .ltl .syntax}
                      > LTLFormula ";"  LTLFormula [right]
```

#### LTLEval

`LTLEval` evaluates the LTL formula over the set of active state labels at **each state** during model simulation.
The result is `LTLFormula` sort.

```{.k .ltl .syntax}
  syntax LTLFormula ::= LTLEval(LTLFormula, Set) [function]
```

#### LTLFinalize

`LTLFinalize` evaluates the LTL formula as the **last step** of model simulation.

```{.k .ltl .syntax}
                      | LTLFinalize(LTLFormula) [function]
```

### KItems

Declare special labels that will be used to establish an order of evaluation.

```{.k .ltl .syntax}
  syntax KItem ::= "#ltlEval"
                 | "#ltlUpdateLabels"
                 | "#ltlStop"
```

```{.k .ltl .syntax}
endmodule
```

## Configuration

The module comes with an internal configuration cell `<ltl>` that contains ltl state data.

```{.k .ltl .config}
module LTL-CONFIG
  imports LTL-SYNTAX
  imports LIST

  configuration
  <ltl>
```

`<formula>` cell contains an LTL formula.
The original LTL formula is placed here at the beginning of evaluation.
It evolves with every evaluation step eventually becoming `True` or `False`.

### Formula

```{.k .ltl .config}
    <formula> $LTL:LTLFormula </formula>
```

### Labels

`<labels>` cell contains a set of `StateLabel` items.
It signals active conditions (atomic predicates) which are true for particular `#ltlEval` step.
Those labels may represent some events or names of predicates in a current state.
See [StateLabel semantics](#statelabel-semantics) for more details.

```{.k .ltl .config}
    <labels> .Set </labels>
```

```{.k .ltl .config}
  </ltl>

endmodule
```

## Semantics

Declare `LTL` module and import `LTL-SYNTAX` and built-in modules that will be used in LTL semantics.

```{.k .ltl .semantics}
module LTL
```

```{.k .ltl .semantics}
  imports LTL-SYNTAX

  imports BOOL
  imports SET
```

### StateLabel semantics

The meaning of the `StateLabel` can be described as a "condition in our program that we care about".
The LTL module defines it as:

```{.k .ltl .semantics}
  rule LTLEval(L:StateLabel, Ls) => True  requires L in Ls
  rule LTLEval(_L:StateLabel, _Ls) => False [owise]
```

Which means if the `StateLabel` is among the set of available state labels, we consider it `True`.
If the label is not present, we consider it `False`.
It is up to the user module to provide consistent and meaningful sets of state labels.

The simplest example is to create state labels corresponding to all global non zero variables before each `LTLEval` call.

### Boolean logic

We define standard boolean logic for our syntax.
All the operators _not_, _or_, _and_, etc behave as expected:

```{.k .ltl .semantics}
  rule ! True         => False [anywhere]
  rule ! False        => True  [anywhere]
  rule True  || _     => True  [anywhere]
  rule _     || True  => True  [anywhere]
  rule False || RHS   => RHS   [anywhere]
  rule LHS   || False => LHS   [anywhere]
  rule False && _     => False [anywhere]
  rule _     && False => False [anywhere]
  rule True  && RHS   => RHS   [anywhere]
  rule LHS   && True  => LHS   [anywhere]
  rule True  -> RHS   => RHS   [anywhere]
  rule _     -> True  => True  [anywhere]
  rule False -> _     => True  [anywhere]
  rule True  ^ True   => False [anywhere]
  rule False ^ False  => False [anywhere]
  rule True  ^ False  => True  [anywhere]
  rule False ^ True   => True  [anywhere]
```

### Boolean ops inside helper functions

We also define boolean operators inside our helper functions:

```{.k .ltl .semantics}
  rule LTLEval(! F, Ls)        => ! LTLEval(F, Ls)
  rule LTLEval(LHS || RHS, Ls) => LTLEval(LHS, Ls) || LTLEval(RHS, Ls)
  rule LTLEval(LHS && RHS, Ls) => LTLEval(LHS, Ls) && LTLEval(RHS, Ls)
  rule LTLEval(LHS -> RHS, Ls) => LTLEval(LHS, Ls) -> LTLEval(RHS, Ls)
  rule LTLEval(LHS ^  RHS, Ls) => LTLEval(LHS, Ls) ^  LTLEval(RHS, Ls)

  rule LTLFinalize(! F) => ! LTLFinalize(F)
  rule LTLFinalize(LHS && RHS) => LTLFinalize(LHS) && LTLFinalize(RHS)
  rule LTLFinalize(LHS || RHS) => LTLFinalize(LHS) || LTLFinalize(RHS)
  rule LTLFinalize(LHS ^ RHS)  => LTLFinalize(LHS) ^  LTLFinalize(RHS)
  rule LTLFinalize(LHS -> RHS) => LTLFinalize(LHS) -> LTLFinalize(RHS)
```

The idea here is that if we get a boolean operation such as _or_ `||` inside our function, it is the same as the function applied to both sides of the formula.

For example if `LTLEval` gets:

```example
LTLEval(a || b, {b, c, d})
```

Where `a || b` is our LTL formula and everything inside `{}` are available state labels.

The above statement is equivalent (and will be rewritten) to

```example
LTLEval(a, {b, c, d}) || LTLEval(b, {b, c, d})
```

Based on the rules we defined in the [StateLabel semantics](#statelabel-semantics) section, it will be further simplified to boolean logic:

```example
False || True
```

Which is reduced to `True`.

In case our helper functions get [LTLResult](#ltlresult) we rewrite it as the result:

```{.k .ltl .semantics}
  rule LTLEval(R:LTLResult, _)  => R
  rule LTLFinalize(R:LTLResult) => R
```

For example if LTLEval gets:

```example
LTLEval(True, {a, b, c})
```

We can ignore the labels and rewrite it as `True`.

### Temporal modal operators

Temporal modal operators have meaning on a sequences of states.
As far as our LTL module is concerned, a new state is when `LTLEval` is called by a user module.
It is up to the user module to decide when to call `LTLEval` and what parameters to provide.

#### _Always_ [] semantics

The meaning of _always_ temporal operator, denoted as `[]`, is that the specific state label has to be present each time we evaluate state labels.
For example, state label `a` must be present at state _one_ and state _two_ and so on until we have checked all the states.
Therefore, we can simplify formula `F` that has `[]` in the following way:

```{.k .ltl .semantics}
  rule LTLEval([] F, Ls)   => LTLEval(F, Ls) && [] F
```

#### _Eventually_ <> semantics

Similarly, temporal operator _eventually_, denoted as `<>`, means that the given label needs to exist in state _one_ or state _two_ and so on.
Therefore, we can write the following rule:

```{.k .ltl .semantics}
  rule LTLEval(<> F, Ls)   => LTLEval(F, Ls) || <> F
```

#### _Multiple_ formulas `;` semantics

<!-- TODO: explain this: -->

```{.k .ltl .semantics}
  rule LTLEval(F1; F2, Ls) => LTLEval(F1, Ls) && F2
```

#### LTLFinalize semantics

When we run out of states, the LTL module expects the user module to call `LTLFinalize` **once and only once** on the remaining formula.

Note that from evaluating `LTLEval` in all states we will have remaining
`&& [] F` and `|| <> F`.
In case of `[]`, to keep the value on the Left Hand Side (LHS) we apply:

```{.k .ltl .semantics}
  rule LTLFinalize([] _F) => True
```

For example, this will rewrite `True && [] F` to `True && True` and further to `True`.
In case of `False` in LHS, the rule will preserve `False` which is the intended behavior.

In case of `<>`, to keep the LHS value we need:

```{.k .ltl .semantics}
  rule LTLFinalize(<> _F) => False
```

This way we get rid of `<>` and preserve the value of computed formula after evaluating all the states.
`True || False` and `False || False` preserves the value on the left hand side in each case.

If ths is the last step of evaluation but we have more formulas, we simply ignore the rest of them:

```{.k .ltl .semantics}
  rule LTLFinalize(F; _) => LTLFinalize(F)
```

The general the idea is to reduce temporal modal operators in our formula to boolean logic in each state.
When our program has nothing left to execute, we compute the final result of our formula.

```{.k .ltl .semantics}
endmodule
```

## Usage example

Checkout [LTL test module](../../tests/semantics/ltl-example.md) for usage examples and tests.
