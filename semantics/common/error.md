# Error module

The purpose of the module is to define and handle errors.

## Syntax

```{.k .error .syntax}
module ERROR-SYNTAX
    imports DOMAINS

    syntax ErrorMeta ::= Id | String
    syntax ErrorMetaList ::= NeList{ ErrorMeta, ","}

    syntax KItem ::= #error(ErrorMetaList)
endmodule
```

## Semantics

```{.k .error .semantics}
module ERROR
    imports ERROR-SYNTAX

endmodule
```
