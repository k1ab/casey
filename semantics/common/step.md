# Step

The purpose of this module is to define common `step` interface.
Steps are used to mark areas in code that can behave differently depending on environment.

## Syntax

```{.k .step .syntax}
module STEP-SYNTAX

    syntax StepType ::= "varChanged"
                      | "varRead"
                      | "threadStart"
                      | "threadEnd"
                      | "threadWait"

    syntax KItem ::= #step(StepType)

    syntax KItem ::= "#varChangedStep"    [macro]
                   | "#varReadStep"       [macro]
                   | "#threadStart"       [macro]
                   | "#threadEnd"         [macro]
                   | "#threadWait"        [macro]

    rule #varChangedStep => #step(varChanged)
    rule #varReadStep => #step(varRead)
    rule #threadStart => #step(threadStart)
    rule #threadEnd => #step(threadEnd)
    rule #threadWait => #step(threadWait)

endmodule
```

## Semantics

```{.k .step .semantics}
module STEP
    imports STEP-SYNTAX

endmodule
```

## Skip steps helper module

This module can be used to skip all "#step" markers.

```{.k .step .semantics}
module STEP-SKIP
    imports STEP-SYNTAX

    rule #step(_) => .

endmodule
```
