# Common C program configuration

## Requirements

We need to set initial "global" frame so we import frame module:

```{.k .pgm .requirements}
requires "../c--/frame.md"
```

## Syntax

```{.k .pgm .syntax}
module PGM-SYNTAX
    imports FRAME-SYNTAX

    syntax Program

    syntax KItem ::= "#endParse"

endmodule
```

## Configuration

```{.k .pgm .config}
module PGM-CONFIG
    imports DOMAINS
    imports PGM-SYNTAX

    configuration
        <k> #newFrame() ~> $PGM:Program ~> #endParse </k>

endmodule
```
