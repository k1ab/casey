# Multi-Threaded C--

Execute C-- source code in multi-threaded environment.

## Requirements

Here we'll use C--, ltl, and multi-threaded components:

```{.k .requirements}
requires "../c--/cmm.md"
requires "../ltl/ltl.md"
requires "../common/pgm.md"
requires "../common/step.md"
requires "../environment/functions-as-threads.md"
```

## Syntax

Define multi-threaded module and include the required components:

```{.k .multi-threaded-cmm .syntax}
module MULTI-THREADED-CMM-SYNTAX
    imports CMM-SYNTAX
    imports FUNCTIONS-AS-THREADS
    //imports LTL-SYNTAX

endmodule
```

## Configuration

```{.k .multi-threaded-cmm .config}
module MULTI-THREADED-CMM-CONFIG
    imports CMM-CONFIG
    imports PGM-CONFIG
    //imports LTL-CONFIG
    imports FUNCTIONS-AS-THREADS-CONFIG

    configuration
        <threadCell/>
        <functionCell/>
        //<ltl/>

endmodule
```

## Semantics

```{.k .multi-threaded-cmm .semantics}
module MULTI-THREADED-CMM
    imports MULTI-THREADED-CMM-SYNTAX
    imports MULTI-THREADED-CMM-CONFIG
    imports CMM
    //imports LTL
    imports FUNCTIONS-AS-THREADS

endmodule
```

## How to build and verify multi-threaded code

Checkout an [example with Peterson](../../tests/environment/peterson.md) algorithm for details on how to build and verify multithreaded algorithms.
