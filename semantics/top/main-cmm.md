# Single threaded C-- with default main function execution

## Requirements

```{.k .main-cmm .requirements}
requires "../c--/cmm.md"
requires "../environment/main.md"
requires "../common/pgm.md"
requires "../common/step.md"
requires "../hook/printf.md"
```

## Syntax

```{.k .main-cmm .syntax}
module MAIN-CMM-SYNTAX
    imports CMM-SYNTAX
    imports MAIN-SYNTAX
    imports PRINTF-SYNTAX

    syntax Stmt ::= Printer
endmodule
```

## Configuration

```{.k .main-cmm .config}
module MAIN-CMM-CONFIG
    imports CMM-CONFIG
    imports PGM-CONFIG

    configuration
        <k/>
        <frameCell/>
        <functionCell/>

endmodule
```

## Semantics

```{.k .main-cmm .semantics}
module MAIN-CMM
    imports MAIN-CMM-SYNTAX
    imports MAIN-CMM-CONFIG
    imports CMM
    imports MAIN
    imports STEP-SKIP
    imports PRINTF

endmodule
```
