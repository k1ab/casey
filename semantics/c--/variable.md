# Variable

This module deals with variables and assignments.

## Requirements

```{.k .variable .requirements}
requires "type.md"
requires "frame.md"
requires "expression/expression.md"

requires "../common/step.md"
requires "../common/pgm.md"
requires "../common/error.md"
```

## Syntax

```{.k .variable .syntax}
module VARIABLE-SYNTAX
    imports DOMAINS-SYNTAX
    imports TYPE-SYNTAX
    imports ERROR-SYNTAX
    imports EXPRESSION-SYNTAX

    syntax Exp ::= Id

endmodule
```

## Private Syntax

```{.k .variable .syntax .private}
module VARIABLE-PRIVATE-SYNTAX
    imports VARIABLE-SYNTAX

    syntax KItem ::= #newVar(Id, Type)
                   | #setVar(Id, TypedValue)
                   | #setVar(Id, TypedValue, Int)
                   | #getVar(Id)
                   | #getVar(Id, Int)

endmodule
```

## Configuration

```{.k .variable .config}
module VARIABLE-CONFIG
    configuration
        <variables>.Map</variables>
endmodule
```

## Semantics

```{.k .variable .semantics}
module VARIABLE
    imports VARIABLE-PRIVATE-SYNTAX

    imports STEP-SYNTAX
    imports PGM-CONFIG

    imports DOMAINS
    imports FRAME
    imports EXPRESSION

    rule T:Type Name:Id = Tv:TypedValue ; => #newVar(Name, T) ~> #setVar(Name, Tv)

    // Declare a variable
    rule T:Type Name:Id ; => #newVar(Name, T)
    // Set value to a declared variable
    rule Name:Id = Tv:TypedValue ; => #setVar(Name, Tv)

    // Variable access rule
    rule Name:Id => #getVar(Name)

    // Working with variables
    rule <k> #newVar(Name:Id, T:Type) => . ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables> M => M [Name <- T, undefined] </variables>
           </frame>
           ...
         </frames>
         <topFrameNum> N </topFrameNum>


    // Start looking at the top frame
    rule <k> #setVar(Name:Id, Val:TypedValue) => #setVar(Name, Val:TypedValue, N) ... </k>
         <topFrameNum> N </topFrameNum>

    // Matched Name in current frame N
    rule <k> #setVar(Name:Id, Inbound:Type, NewVal:Value, N:Int) => . ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables>... Name |-> (CurrentType:Type, _:Value => CurrentType, NewVal) ...</variables>
           </frame>
           ...
         </frames>
         requires #canBeAssignedTo(CurrentType, Inbound)

    // Matched Name in current frame N but types are wrong
    rule <k> #setVar(Name:Id, Inbound:Type, _, N:Int) => #error("Invalid assignment type") ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables>... Name |-> Current:Type, _:Value ...</variables>
           </frame>
           ...
         </frames>
         requires notBool #canBeAssignedTo(Current, Inbound)

    // Didn't find Name in frame N. Trying the next one.
    rule <k> #setVar(Name:Id, Inbound:TypedValue, N:Int) => #setVar(Name, Inbound, N -Int 1) ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables> Vars </variables>
           </frame>
           ...
         </frames>
         requires N >=Int 0
                  andBool notBool Name in_keys(Vars)

    // We didn't fine the variable so we set an error.
    rule <k> #setVar(Name:Id, _:TypedValue, -1) => #error("Variable not found", Name) ... </k>
         [owise]

    // Start looking at the top frame
    rule <k> #getVar(Name:Id) => #getVar(Name, N) ... </k>
         <topFrameNum> N </topFrameNum>

    // Didn't find Name in frame N. Trying the next one.
    rule <k> #getVar(Name:Id, N:Int) => #getVar(Name, N -Int 1) ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables> Vars </variables>
           </frame>
           ...
         </frames>
         requires N >=Int 0
                  andBool notBool Name in_keys(Vars)

    // Matched Name in current local frame N
    rule <k> #getVar(Name:Id, N:Int) => Val ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables>... Name |-> Val ...</variables>
           </frame>
           ...
         </frames>
          requires N =/=Int 0 andBool #isDefined(Val)

    // Matched Name in global frame
    rule <k> #getVar(Name:Id, N:Int) => #varReadStep ~> Val ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables>... Name |-> Val ...</variables>
           </frame>
           ...
         </frames>
          requires N ==Int 0 andBool #isDefined(Val)

    // Found undefined value
    rule <k> #getVar(Name:Id, N:Int) => #error("Access variable before assignment", Name) ... </k>
         <frames>
           ...
           <frame>
             <fid> N </fid>
             <variables>... Name |-> Val ...</variables>
           </frame>
           ...
         </frames>
          requires notBool #isDefined(Val)


    rule <k> #getVar(Name:Id, -1) => #error("Variable not found", Name) ... </k>
         [owise]

endmodule
```
