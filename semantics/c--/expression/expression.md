# Expression module

This module is a holder for all expression components.
For example use `requires "expression/expression.md"` and `import EXPRESSION`
to include all expression syntax and semantics.

## Components

This module contains the following components:

* [Operator](operator.md):
  * Arithmetic
  * Relational (comparison)
  * Logical
* [Control](control.md):
  * Conditional
  * Assignment statements
* [Constants](constant.md)
  * Integer

```{.k .expression .requirements}
requires "operator.md"
requires "control.md"
requires "constant.md"
```

## Syntax and semantics

As stated above syntax and semantics are provided by the sub-modules.

```{.k .expression .syntax}
module EXPRESSION-SYNTAX
    imports OPERATOR-SYNTAX
    imports CONTROL-SYNTAX
    imports CONSTANT
endmodule

module EXPRESSION
    imports EXPRESSION-SYNTAX

    imports OPERATOR
    imports CONTROL
    imports CONSTANT

endmodule
```
