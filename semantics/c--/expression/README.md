# Expression

In C language an [expression](https://en.cppreference.com/w/c/language/expressions) is a sequence of operators and their operands, that specifies a computation.
C-- relies on a collection of modules to provide syntax and semantics for expressions.
The module collection provides `Exp` sort that represents all user facing expressions and
`Stmt` sort with assignment statements.
User modules can extend `Exp` and `Stmt` sorts or use them as is.

Expression module collection depends on several [external modules](exp-external.md).
It also requires syntax and rules that can split statements.
That requirement comes from the [control](control.md) sub-module because it reduces and reorganizes statements.

## Section navigation

<!--nav-->
* [About](README.md)
* [Expression module](expression.md)
* [Operator](operator.md)
* [Control](control.md)
* [Constants](constant.md)
* [External dependencies](exp-external.md)
