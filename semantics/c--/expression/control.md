# Control

This module defines supported control statements.

## Requirements

We are working with typed values so we need the [type](../type.md) module.
We also expect control statements to work with different operators so it requires the [operator](operator.md) module.
Control statements rearrange other statements so it requires the [statement](../statement.md) module as well.

```{.k .control .syntax}
requires "exp-external.md"
requires "operator.md"
requires "../../common/pgm.md"
```

## Syntax

```{.k .control .syntax}
module CONTROL-SYNTAX
    imports TYPE-SYNTAX
    imports OPERATOR-SYNTAX
    imports STATEMENT-SYNTAX
```

This module does not explicitly require expressions but it expects user modules to define `Exp` and `AssignmentExpression`.

```{.k .control .syntax}
    syntax Exp
    syntax AssignmentExpression
```

We define available selection statements

```{.k .control .syntax}
    syntax Stmt ::= "continue" ";"
                  | "break" ";"

    syntax SelectionStatement ::= "if" "(" Exp ")" "{" Stmt "}" "else" "{" Stmt "}"     [strict(1)]
                                | "if" "(" Exp ")" "{" Stmt "}"                         [strict(1)]
```

and iterations statements.

```{.k .control .syntax}
    syntax IterationStatement ::= "while" "(" Exp ")" "{" Stmt "}"
                                | "do" "{" Stmt "}" "while" "(" Exp ")" ";"
                                | "for" "(" AssignmentExpression ";" Exp ";" AssignmentExpression ")" "{" Stmt "}"
```

We expend statements sort with control subsorts:

```{.k .control .syntax}

    syntax Stmt ::= SelectionStatement
                  | IterationStatement
```

```{.k .control .syntax}
endmodule
```

## Internal syntax

Control module contains internal syntax available for semantics rules but not
available for users writing `C--`.


```{.k .control .internal .syntax}
module CONTROL-INTERNAL-SYNTAX
    imports CONTROL-SYNTAX
```

We add `#continue` and `#break` markers so we can skip statements when we encounter
`continue;` and `break;`.
We also define an internal `#while` so we can put the loop exit marker `#break` only once.

```{.k .control .internal .syntax}
    syntax Stmt ::= "#continue"
                  | "#break"
                  | "#debug"

    syntax IterationStatement ::= "#while" "(" Exp ")" "{" Stmt "}"
```

```{.k .control .internal .syntax}
endmodule
```

## Semantics

We import syntax module for control and rules for other required modules.

```{.k .control .semantics}
module CONTROL
    imports CONTROL-INTERNAL-SYNTAX
    imports TYPE
    imports OPERATOR
    imports STATEMENT
    imports PGM-CONFIG
```

Similar to `if` statements described in [the standard](https://en.cppreference.com/w/c/language/if),
an expression evaluated to `0` is considered `false`.
Everything else is considered to be `true`.

```{.k .control .semantics}

    rule if (T:Type, V:Int) {S} else {_} => S
        requires #toKBool(T, V)

    rule if (T:Type, V:Int) {_} else {S} => S
        requires notBool #toKBool(T, V)

    rule if (T:Type, V:Int) {S} => S
        requires #toKBool(T, V)

    rule if (T:Type, V:Int) {_} => .
        requires notBool #toKBool(T, V)
```

We express `while` statement as `if` statement followed by another `while` statement.
We also add a helper continue statement similar to how it is
[described in the standard](https://en.cppreference.com/w/c/language/continue).
This way we can skip the rest of the statement when we encounter `continue;`.
Note that we also add a `#break` marker in order to support
[exiting loops](https://en.cppreference.com/w/c/language/break)
with a break statement.

```{.k .control .semantics}
    rule  while (E) { S:Stmt } => if (E) { S #continue #while(E) {S} } #break
    rule #while (E) { S:Stmt } #as W => if (E) { S #continue W }
```

When we encounter `break;` we skip the next statement until we encounter our `#break` marker.

```{.k .control .semantics}
    rule break; ~> (S:Stmt => .)
        requires S =/=K #break
```

Our two base cases when we do not encounter `break;` statement and when we encounter it
at the end of the loop:

```{.k .control .semantics}
    rule #break => .
    rule break; ~> #break => .
```

We also skip statements when we encounter `continue;` until we encounter `#continue` marker.
The rule looks different from the rule that skips statements after `break;` because the statements
between those markers are not unpacked yet and we can skip all of them in one substitution.

```{.k .control .semantics}
    rule continue; ~> _:Stmt ~> #continue Rest => Rest
```

Here we also handle two base cases when we have no `continue;` statement and when it is
preceding immediately before `#continue` marker.

```{.k .control .semantics}
    rule #continue ~> S:Stmt => S
    rule continue; ~> #continue Rest => Rest
```

We express the other iteration statements in terms of the `while` statement.
In case of the `do` statement, we take special care when handling `break` statements inside,
because the rewriting step places an instance of the loop body outside the `while` loop.

```{.k .control .semantics}
    rule do { S } while (E); => S #continue #while (E) { S } #break ~> #break
```

In case of the `for` loop we need to make sure that we do not skip the iterator at the end.
Therefore, we insert another end-of-body marker before the iterator statement.

```{.k .control .semantics}
    rule for (Init; E; Iter) { S } => Init; #while(E){S #continue Iter;} #break
```

```{.k .control .semantics}
endmodule
```
