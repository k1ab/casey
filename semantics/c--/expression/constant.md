# Constant module

## Requirements

```{.k .constant .requirements}
requires "exp-external.md"
```

## Syntax

```{.k .constant .syntax}
module CONSTANT-SYNTAX
    imports TYPE-SYNTAX
```

```{.k .constant .syntax}
    syntax Exp ::= Int
endmodule
```

## Semantics

```{.k .constant .semantics}
module CONSTANT
    imports CONSTANT-SYNTAX
    imports TYPE

    rule I:Int => #toTypedValue(I, int8_t, int16_t, int32_t, int64_t)
endmodule
```
