# Operator

This module defines supported operators.

Users of this module such as [expression](expression.md) can rely on `Exp` and `Stmt` sorts.
The module extends `Exp` sort with arithmetic, relational, and Boolean operators.
It also provides `Stmt` sort that defines statements containing assignment operator.

## Requirements

This module expects:

* Typed values from [type](../type.md) module
* Errors from [error](../../common/error.md) module

```{.k .operator .requirements}
requires "exp-external.md"
```

## Syntax

We start with initializing the module and importing all the required syntax modules.

```{.k .operator .syntax}
module OPERATOR-SYNTAX
    imports DOMAINS-SYNTAX
    imports TYPE-SYNTAX
    imports ERROR-SYNTAX

    syntax Exp
```

### Assignment statements

We start with type declaration expression.
It matches any variable declaration such as `int abc`.

```{.k .operator .syntax}
    syntax DeclarationExpression ::= Type Id
```

Based on variable type declaration we define assignment.
Our syntax supports assignment together with variable declaration such as `int abc = 10`.
It also supports simple variable assignment such as `abc = 10`

```{.k .operator .syntax}
    syntax AssignmentExpression ::= DeclarationExpression "=" Exp   [right]
                                  | Id "=" Exp                      [right]
```

We expose variable declaration and assignment via `Stmt` sort.

```{.k .operator .syntax}
    syntax Stmt ::= AssignmentExpression ";"
                  | DeclarationExpression ";"
                  | Exp ";"

```

### Typed values and casting

Operators in this module work only with typed values so `TypedValue` defined in [type](../type.md) module
must be a sub-sort of `Exp` sort.

```{.k .operator .syntax}
    syntax Exp ::= TypedValue [literal]
```

`TypedValue` sort is an internal representation of typed values.
We define casting as a way for a user to specify type of a value.

```{.k .operator .syntax}
    syntax TypeCast ::= "(" Type ")" Value    [typecast]

    syntax Exp ::= TypeCast
```

Below, [semantic rules](#typecast) validate that the casting is allowed.

### Operator expressions

According to [C reference](https://en.cppreference.com/w/c/language/eval_order)
the order of evaluation of the sub-expressions within any expression is unspecified.
Therefore, we use `strict` without specifying which sub-expression should evaluate first.
Here we define syntax for all possible operators.

```{.k .operator .syntax}
    syntax Exp ::= "(" Exp ")"      [bracket, atom]
                 | "!" Exp          [not]
                 | Exp "*" Exp      [strict, mult]
                 | Exp "/" Exp      [strict, div]
                 | Exp "%" Exp      [strict, rem]
                 | Exp "+" Exp      [strict, plus]
                 | Exp "-" Exp      [strict, minus]
                 | Exp "<<" Exp     [strict, bshl]
                 | Exp ">>" Exp     [strict, bshr]
                 | Exp "&&" Exp     [strict, and]
                 | Exp "||" Exp     [strict, or]
                 | Exp ">" Exp      [strict, lt]
                 | Exp "<" Exp      [strict, gt]
                 | Exp "<=" Exp     [strict, lte]
                 | Exp ">=" Exp     [strict, gte]
                 | Exp "==" Exp     [strict, eq]
                 | Exp "!=" Exp     [strict, neq]
                 | Exp "&" Exp      [strict, band]
                 | Exp "^" Exp      [strict, bxor]
                 | Exp "|" Exp      [strict, bor]

```

We still need to set operator order as [described in the reference](https://en.cppreference.com/w/c/language/operator_precedence).
K Framework has a special syntax to define explicit [priority and associativity declarations](https://kframework.org/k-distribution/k-tutorial/1_basic/04_disambiguation/).

```{.k .operator .syntax}
    syntax priorities literal
           > atom
           > typecast not       // group 2
           > mult div rem       // group 3
           > plus minus         // group 4
           > bshl bshr          // group 5
           > lt gt lte gte      // group 6
           > eq neq             // group 7
           > band               // group 8
           > bxor               // group 9
           > bor                // group 10
           > and                // group 11
           > or                 // group 12

    syntax right typecast not
    syntax left mult div rem
    syntax left plus minus
    syntax left lt gt lte gte
    syntax left eq neq

    syntax right not
    syntax right typecast

    syntax left mult
    syntax left div
    syntax left rem
    syntax left plus
    syntax left minus
    syntax left lt
    syntax left gt
    syntax left lte
    syntax left gte
    syntax left eq
    syntax left neq
    syntax left band
    syntax left bxor
    syntax left bor
    syntax left and
    syntax left or
```

This declaration is equivalent to the precedence table where `>` separates operator groups.
For example the group with `mult` and `div` have higher priority than the group with `plus` and `minus`.
Keywords `left` or `right` define associativity within a group.

```{.k .operator .syntax}
endmodule
```

## Semantics

```{.k .operator .semantics}
module OPERATOR
    imports OPERATOR-SYNTAX

    imports DOMAINS
    imports TYPE
    imports ERROR
```

### Assignment

Evaluate expressions on the right side first.

```{.k .operator .semantics}
    context _:Id = HOLE:Exp ;
    context _:Type _:Id = HOLE:Exp ;
```

### Typecast

Define rules for typecasting:

```{.k .operator .semantics}
    rule ( T:Type ) V => #error("Invalid casting")
        requires notBool #canCastTo(T, V)
    rule ( T:Type ) V => T, V
        requires #canCastTo(T, V)
```

### Unused expression result

Evaluate expressions until we get `KResult`.
This enables function calls that might update global variables.

```{.k .statement .syntax}
    context HOLE:Exp;
```

The result of void functions can be ignored:

```{.k .statement .syntax}
    rule void, undefined ; => .
```

Expressions that can't be evaluated further should raise an error.
For example, `int8_t , 5 ;` will have no effect.
In `C--` the interpreter will stop execution with an error.

```{.k .statement .syntax}
    rule _:TypedValue ; => #error("Expression result unused") [owise]
```

### Arithmetic

Even though compilers sometimes are okay with integer overflow, we disallow it.
For example, while expressions like `int16_t a = 127 + 1` will compile and work as expected,
we will stop execution here with an error.
In order for the expression above to work, one would need to explicitly cast `127` to 16 bit int:
`int16_t a = (int16_t) 127 + 1`.
The reason for this is to avoid ambiguity when assigning to smaller bit size.
GCC and Clang will accept `int8_t a = 127 + 1` but will wrap the value to `-128`.
This gets even more confusing with `int64_t` because
`int64_t a = INT32_MAX + 1` will wrap back to negative values even though the result fits into 64 bit int.
This way C-- still can be compiled by standard compiler but demands explicit typecast around integer overflow.

Here we define rules for integer overflow errors:

```{.k .operator .semantics}
    rule T1:Type, V1:Int + T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 +Int V2)
    rule T1:Type, V1:Int - T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 -Int V2)
    rule T1:Type, V1:Int * T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 *Int V2)

    rule T1:Type, V1:Int & T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 &Int V2)
    rule T1:Type, V1:Int ^ T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 xorInt V2)
    rule T1:Type, V1:Int | T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 |Int V2)
```

Note that left shift operator `x << y` is equivalent to `x * (2 pow y)` so we also need to check for int overflow.

```{.k .operator .semantics}
    rule T1:Type, V1:Int << T2:Type, V2:Int => #error("Integer overflow")
        requires notBool #canCastTo(#supertype(T1, T2), V1 <<Int V2)
```

If values are within supported type, we perform the calculation and assign the biggest bit size.

```{.k .operator .semantics}
    rule T1:Type, V1:Int + T2:Type, V2:Int => #supertype(T1, T2), V1 +Int V2
        requires #canCastTo(#supertype(T1, T2), V1 +Int V2)
    rule T1:Type, V1:Int - T2:Type, V2:Int => #supertype(T1, T2), V1 -Int V2
        requires #canCastTo(#supertype(T1, T2), V1 -Int V2)
    rule T1:Type, V1:Int * T2:Type, V2:Int => #supertype(T1, T2), V1 *Int V2
        requires #canCastTo(#supertype(T1, T2), V1 *Int V2)

    rule T1:Type, V1:Int & T2:Type, V2:Int => #supertype(T1, T2), V1 &Int V2
        requires #canCastTo(#supertype(T1, T2), V1 &Int V2)
    rule T1:Type, V1:Int ^ T2:Type, V2:Int => #supertype(T1, T2), V1 xorInt V2
        requires #canCastTo(#supertype(T1, T2), V1 xorInt V2)
    rule T1:Type, V1:Int | T2:Type, V2:Int => #supertype(T1, T2), V1 |Int V2
        requires #canCastTo(#supertype(T1, T2), V1 |Int V2)

    rule T1:Type, V1:Int << T2:Type, V2:Int => #supertype(T1, T2), V1 <<Int V2
        requires #canCastTo(#supertype(T1, T2), V1 <<Int V2)
```

Since right shift operator `x >> y` is equivalent to `x / (2 pow y)` we will not get int overflow at this point.

```{.k .operator .semantics}
    rule T1:Type, V1:Int >> T2:Type, V2:Int => #supertype(T1, T2), V1 >>Int V2
```

For division and module rules we check for 0 values:

```{.k .operator .semantics}
    rule _:Type, _:Int / _:Type, V2:Int => #error("Division by zero")
        requires V2 ==Int 0
    rule T1:Type, V1:Int / T2:Type, V2:Int => #supertype(T1, T2), V1 /Int V2
        requires V2 =/=Int 0

    rule _:Type, _:Int % _:Type, V2:Int => #error("Division by zero")
        requires V2 ==Int 0
    rule T1:Type, V1:Int % T2:Type, V2:Int => #supertype(T1, T2), V1 %Int V2
        requires V2 =/=Int 0
```

### Relational

Here we can rely on built-in `Int` relational operators which return internal Boolean value.
Our helper `#toTypedValue` knows how to convert internal Boolean to user facing representation.

```{.k .operator .semantics}
    rule _:Type, V1:Int <  _:Type, V2:Int => #toTypedValue(V1  <Int  V2)
    rule _:Type, V1:Int >  _:Type, V2:Int => #toTypedValue(V1  >Int  V2)
    rule _:Type, V1:Int <= _:Type, V2:Int => #toTypedValue(V1 <=Int  V2)
    rule _:Type, V1:Int >= _:Type, V2:Int => #toTypedValue(V1 >=Int  V2)
    rule _:Type, V1:Int == _:Type, V2:Int => #toTypedValue(V1 ==Int  V2)
    rule _:Type, V1:Int != _:Type, V2:Int => #toTypedValue(V1 =/=Int V2)
```

### Logical

Similar to [relational](#relational) section we rely on internal Boolean to support logical operators.
The only trick here is to convert values to internal Boolean and change the result back to user facing representation.

```{.k .operator .semantics}
    rule T1:Type, V1:Int && T2:Type, V2:Int => #toTypedValue(#toKBool(T1, V1) andBool #toKBool(T2, V2))
    rule T1:Type, V1:Int || T2:Type, V2:Int => #toTypedValue(#toKBool(T1, V1) orBool #toKBool(T2, V2))
```

Before we can apply the `not` operator we need to evaluate the expression first so we can support variable lookup.

```{.k .operator .semantics}
    context ! HOLE:Exp
```

When we got the actual value we can negate it.

```{.k .operator .semantics}
    rule ! T1:Type, V1:Int => #toTypedValue(notBool #toKBool(T1, V1))
```

```{.k .operator .semantics}
endmodule
```
