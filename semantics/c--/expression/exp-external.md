# External dependencies

All expression sub-modules should import this module for their external dependencies.

```{.k .exp-external.requirements}
requires "../type.md"
requires "../statement.md"
requires "../../common/error.md"
```

After `requires "exp-external.md"` expression sub-modules can import individual modules such as `ERROR` or all external modules at once.

For convenience we provide all external syntax:

```{.k .exp-external.requirements}
module EXP-EXTERNAL-SYNTAX
    imports TYPE-SYNTAX
    imports ERROR-SYNTAX
    imports STATEMENT-SYNTAX
endmodule
```

And semantics from all external modules:

```{.k .exp-external.requirements}
module EXP-EXTERNAL
    imports EXP-EXTERNAL-SYNTAX
    imports TYPE
    imports ERROR
endmodule
```
