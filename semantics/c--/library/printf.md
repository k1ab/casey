# Printf

Printf implementation that behaves almost like real C `printf`.
A better way to expose `printf` is to add a hook to a real printf
since we don't need to test it or consider it in our models.

## Requirements

In order to work with `<k>` cell we import it from the common program configuration.

```{.k .printf .requirements}
requires "../../common/pgm.md"
```

We also expect `printf` to work with variables.

```{.k .printf .requirements}
requires "../variable.md"
```


## Syntax

```{.k .printf .syntax}
module PRINTF-SYNTAX
    imports DOMAINS

    //imports VARIABLE-SYNTAX

    syntax Printable ::= Int | String | Bool
    syntax PrintableList ::= List{Printable, ","}
    syntax Printer ::= "printf" "(" String ")" ";"
                        | "printf" "(" String "," PrintableList")" ";"
                        | "#print" "(" Printable ")"
                        | "#print" "(" String "," Printable "," Int "," PrintableList")"

endmodule
```

## Configuration

```{.k .printf .config}
module PRINTF-CONFIG
    imports PRINTF-SYNTAX

    configuration
        <out color="Orchid" stream="stdout"> .List </out>

endmodule
```

## Semantics

```{.k .printf .semantics}
module PRINTF
    imports PRINTF-CONFIG
    imports PGM-CONFIG

    rule printf(S:String); => #print(S)
    rule printf(S:String, _); => .
      requires lengthString(S) ==Int 0

    rule printf(S:String, I:Printable, Is); =>
         #print(S, I, findString(S, "%", 0), Is)

    rule #print(Str, I, Match, Is) =>
         #print(substrString(Str, 0, Match)) ~>
         #print(I) ~>
         printf(substrString(Str, Match +Int 2, lengthString(Str)), Is);
      requires Match >=Int 0

    rule #print(Str, _I, _Match, _Is) => #print(Str)

    rule <k> #print(P:Printable) => . ... </k>
         <out> ... .List => ListItem(P) </out>
endmodule
```
