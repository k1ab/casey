# Statement

## Syntax

```{.k .statement .syntax}
module STATEMENT-SYNTAX
    imports TYPE-SYNTAX

    syntax Exp
    syntax Stmt ::= Stmt Stmt            [right]


endmodule
```

## Semantics

```{.k .statement .syntax}
module STATEMENT
    imports STATEMENT-SYNTAX
```

Here we split statements into chunks that we can process.

```{.k .statement .syntax}
    rule S1:Stmt S2:Stmt => S1 ~> S2    [structural]
```

```{.k .statement .syntax}
endmodule
```
