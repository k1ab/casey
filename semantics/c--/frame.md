# Frame

## Requirements

Each frame has its own local variables so we need to import variables module:

```{.k .frame .requirements}
requires "variable.md"
```

## Syntax

```{.k .frame .requirements}
module FRAME-SYNTAX

    syntax Stmt

    syntax KItem ::= #newFrame()
                   | #delFrame()
endmodule
```

## Configuration

```{.k .frame .config}
module FRAME-CONFIG
    imports DOMAINS
    imports VARIABLE-CONFIG

    configuration
        <frameCell>
            <frames>
                <frame multiplicity="*" type="Map">
                    <fid> 0 </fid>
                    <variables/>
                </frame>
            </frames>
            <topFrameNum> -1 </topFrameNum>
        </frameCell>
endmodule
```

## Semantics

```{.k .frame .semantics}
module FRAME
    imports FRAME-SYNTAX
    imports FRAME-CONFIG
    imports PGM-CONFIG

    rule <k> #newFrame() => . ... </k>
         <frames> (.Bag =>
           <frame>
             <fid> N +Int 1</fid>
             <variables> .Map </variables>
           </frame>)
           ...
         </frames>
         <topFrameNum> N => N +Int 1 </topFrameNum>

    rule <k> #delFrame() => . ... </k>
         <frames> (
           <frame>
             <fid> N </fid>
             ...
           </frame>
           => .Bag)
           ...
         </frames>
         <topFrameNum> N => N -Int 1 </topFrameNum>
            requires N >=Int 0

endmodule
```
