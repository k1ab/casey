# Include

## Syntax

```{.k .include .syntax}
module INCLUDE-SYNTAX
    imports DOMAINS

    syntax Stmt
    syntax Program ::= Includes Stmt

    syntax Lib ::= "stdio.h"   [token]
                 | "stdint.h"  [token]

    syntax Include ::= "#include" "<" Lib ">"
    syntax Includes ::= List{Include, ""}

endmodule
```

## Semantics

```{.k .include .syntax}
module INCLUDE
    imports INCLUDE-SYNTAX

    rule _:Includes K => K  [structural]

endmodule
```
