# Type module

Users of this module should expect [`Type`](#type), [`TypedValue`](#typedvalue), and [helper](#helper-functions) functions implementation.
`Type` sort contains all supported types like `int`, `int8_t`, `void`, etc.
`TypedValue` is an internal representation of a value with a type.
It is up to the user modules to take an accepted type declaration and convert it to internal `TypedValue`.
For example, when user module encounters `int i = 100;` the value `100` should be converted to `int8_t, 100`.
Type module provides all the tools to do that but doesn't do it automatically.

## Requirements

The module is a basic building block of C-- so it doesn't require other C-- modules.

## Syntax

```{.k .type .syntax}
module TYPE-SYNTAX
    imports DOMAINS-SYNTAX
    imports ERROR-SYNTAX
```

### IntType

We start with define accepted integer types.
In C code this is equivalent to `#include <stdint.h>` that includes [fixed integer types](https://en.cppreference.com/w/c/types/integer).
Note that we implement only limited subset of `stdint.h`

```{.k .type .syntax}
    syntax IntType ::= "int8_t"
                     | "int16_t"
                     | "int32_t"
                     | "int64_t"
                     | "uint8_t"
                     | "uint16_t"
                     | "uint32_t"
                     | "uint64_t"
```

Common compiler assumption is that `int` means `int32_t`.
In our definition we declare `int` type as a `macro` to `int32_t`.
Whenever we see `int` type, we rewrite it into `int32_t`.

```{.k .type .syntax}
    syntax IntType ::= "int"  [macro]
    rule int => int32_t
```

### Type

This module provides `Type` sort that includes all other supported subtypes.

```{.k .type .syntax}
    syntax Type ::= IntType
                  | "void"
```

There is no user facing syntax to declare a list of types but we need to declare `Types` sort for internal use.
This is done using builtin `List`.

```{.k .type .syntax}
    syntax Types ::= List{Type, ","}
```

### Value

Here we declare a sort called `Value` that holds possible values.
For all integer we can use built in `Int` sort.
We also declare possible `undefined` value which can be used for functions that don't return a value or for variables initialized without assignment.

```{.k .type .syntax}
    syntax Value ::= Int | "undefined"
```

### TypedValue

`TypedValue` is an internal representation of a value that has a type.
This is done in order to distinguish between user declared types or castings and typed values that we already validated.
Here we also declare internal functions (by convention names start with `#` symbol) that can convert into `TypedValue` sort.

```{.k .type .syntax}
    syntax TypedValue ::= Type "," Value
```

### Helper functions

We declare a number of internal helper functions that other modules can use to work with types.

```{.k .type .syntax}
    syntax Type ::= #supertype(Type, Type)            [function]
                  | #signedForBitwidth(Int)           [function]
                  | #unsignedForBitwidth(Int)         [function]

    syntax TypedValue ::= #toTypedValue(Int, Types)   [function]
                        | #toTypedValue(Bool)         [function]

    syntax Int ::= #bitwidth(Type)                    [function]

    syntax Bool ::= #isSigned(Type)                   [function]
                  | #isUnsigned(Type)                 [function]
                  | #canBeAssignedTo(Type, Type)      [function]
                  | #canCastTo(Type, Int)             [function]
                  | #inRange(Int, Int, Int)           [function]
                  | #isDefined(TypedValue)            [function]
                  | #toKBool(Type, Int)               [function]
```

We expect `TypedValue` to be the result of any [evaluation rules](https://kframework.org/k-distribution/k-tutorial/1_basic/14_evaluation_order/)
so we declare it to be a sub-sort of built in `KResult` sort.

```{.k .type .syntax}
    syntax KResult ::= TypedValue
```

```{.k .type .syntax}
endmodule
```

## Semantics

Note that all the rules here describe helper function semantics.
This is done to abstract away lower level type operations.
We expect other modules to use the Type module in order to implement type related semantics.

```{.k .type .semantics}
module TYPE
    imports TYPE-SYNTAX
    imports DOMAINS
```

### #isSigned and #isUnsigned

We start with detecting `signed` or `unsigned` integer class.
Even though user modules might not implement Boolean values we declare rules that use built in Boolean.
User module can declare a rule that uses for example `requires #isSigned(Var)` in its own rules.

```{.k .type .semantics}
    rule #isSigned(int8_t) => true
    rule #isSigned(int16_t) => true
    rule #isSigned(int32_t) => true
    rule #isSigned(int64_t) => true
    rule #isSigned(uint8_t) => false
    rule #isSigned(uint16_t) => false
    rule #isSigned(uint32_t) => false
    rule #isSigned(uint64_t) => false
    rule #isUnsigned(I) => notBool #isSigned(I)
```

### #bitwidth

Next we'll define rules to convert the integer type to integer bits size.
This enables using built in functions when working with integer types.
For example we can compare 8 and 16 bit integer types and pick which one is bigger.

```{.k .type .semantics}
    rule #bitwidth(int8_t) => 8
    rule #bitwidth(int16_t) => 16
    rule #bitwidth(int32_t) => 32
    rule #bitwidth(int64_t) => 64
    rule #bitwidth(uint8_t) => 8
    rule #bitwidth(uint16_t) => 16
    rule #bitwidth(uint32_t) => 32
    rule #bitwidth(uint64_t) => 64
```

### #signedForBitwidth

We provide the revers of `#bitwidth` function.
Given a number of bits we detect the type name.
It would have been possible to simply reverse the rules above but we want to allow broader input range.
In case the user module does some kind of calculation an attempts to convert values like 6, 15, etc.

```{.k .type .semantics}
    rule #signedForBitwidth(Bits) => int8_t
      requires Bits >=Int 0 andBool Bits <=Int 8
    rule #signedForBitwidth(Bits) => int16_t
      requires Bits >=Int 9 andBool Bits <=Int 16
    rule #signedForBitwidth(Bits) => int32_t
      requires Bits >=Int 17 andBool Bits <=Int 32
    rule #signedForBitwidth(Bits) => int64_t
      requires Bits >=Int 33 andBool Bits <=Int 64

    rule #unsignedForBitwidth(Bits) => uint8_t
      requires Bits >=Int 0 andBool Bits <=Int 8
    rule #unsignedForBitwidth(Bits) => uint16_t
      requires Bits >=Int 9 andBool Bits <=Int 16
    rule #unsignedForBitwidth(Bits) => uint32_t
      requires Bits >=Int 17 andBool Bits <=Int 32
    rule #unsignedForBitwidth(Bits) => uint64_t
      requires Bits >=Int 33 andBool Bits <=Int 64
```

### #supertype

Now given two types we need to pick the bigger "bitness" type.
For example when given `int8_t` and `int16_t` the function should return `int16_t`.
The next set of rules uses previously defined helper functions and builtin integer function `maxInt`.

```{.k .type .semantics}
    rule #supertype(I1, I2) => #signedForBitwidth(maxInt(#bitwidth(I1), #bitwidth(I2)))
      requires #isSigned(I1) andBool #isSigned(I2)
    rule #supertype(I1, I2) => #unsignedForBitwidth(maxInt(#bitwidth(I1), #bitwidth(I2)))
      requires #isUnsigned(I1) andBool #isUnsigned(I2)
```

### #canBeAssignedTo

Check if the second type `T2` can be assigned to the first type `T1`.
This is useful when checking if we can assign 16 bit int into 8 bit int.

```{.k .type .semantics}
    rule #canBeAssignedTo(T1, T2) => #bitwidth(#supertype(T1, T2)) ==Int #bitwidth(T1)
```

### #canCastTo

The following set of rules checks if a value fits into a specific integer type.

```{.k .type .semantics}
    rule #inRange(V:Int, L:Int, H:Int) => (L <=Int V) andBool (V <=Int H)

    rule #canCastTo(int8_t, V:Int)   => #inRange(V, -128, 127)
    rule #canCastTo(int16_t, V:Int)  => #inRange(V, -32768, 32767)
    rule #canCastTo(int32_t, V:Int)  => #inRange(V, -2147483648, 2147483647)
    rule #canCastTo(int64_t, V:Int)  => #inRange(V, -9223372036854775808, 9223372036854775807)
    rule #canCastTo(uint8_t, V:Int)  => #inRange(V, 0, 255)
    rule #canCastTo(uint16_t, V:Int) => #inRange(V, 0, 65535)
    rule #canCastTo(uint32_t, V:Int) => #inRange(V, 0, 4294967295)
    rule #canCastTo(uint64_t, V:Int) => #inRange(V, 0, 18446744073709551615)
```

### #toTypedValue

Based on `#canCastTo` helper function, `#toTypedValue` can detect type from a value.
`#toTypedValue` has multiple signatures.
One of those signatures is an integer and a list of integer types that will be tried one by one.
For example, `#toTypedValue(128, int8_t, int16_t)` will return `int16_t`
because 128 does not fit into 8 bit signed integer.

```{.k .type .semantics}
    rule #toTypedValue(I, T, _) => T, I
      requires #canCastTo(T, I)

    rule #toTypedValue(I, _, Rest) => #toTypedValue(I, Rest) [owise]
```

If the function gets an integer value with an empty list of types we consider it to be of type `void` with
`undefined` value.
Depending on circumstances other modules can interpret it as an error.

```{.k .type .semantics}
    rule #toTypedValue(_, .Types) => void, undefined
```

`#toTypedValue` is also responsible for converting built-in Boolean values into typed integers
since C language language treats integers as Boolean values.

```{.k .type .semantics}
    rule #toTypedValue(true) => int32_t, 1
    rule #toTypedValue(false) => int32_t, 0
```

### #toKBool

`#toKBool` function converts typed integers to built-in Boolean.
Zero is converted to false.
Everything else is assumed to be true.

```{.k .type .semantics}
    rule #toKBool(_, I:Int) => false
      requires I ==Int 0
    rule #toKBool(_, I:Int) => true
      requires I =/=Int 0
```

### #isDefined

The next set of helper rules is designed to work with `undefined` value.

```{.k .type .semantics}
    rule #isDefined(_, undefined) => false
    rule #isDefined(_,_) => true [owise]
```

```{.k .type .semantics}
endmodule
```
