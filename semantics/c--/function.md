# Function

The purpose of the module is to define functions syntax and semantics.

## Requirements

Function module interacts with types, frames, and variables.

```{.k .function .requirements}
requires "type.md"
requires "frame.md"
requires "variable.md"
```

We'll also need to interact with the `<k>` cell and error module:

```{.k .function .requirements}
requires "../common/pgm.md"
requires "../common/error.md"
```

## Syntax

Function syntax depends on the domains library as well as type, variable, and frame.

```{.k .function .syntax}
module FUNCTION-SYNTAX
    imports DOMAINS-SYNTAX
    imports TYPE-SYNTAX
    imports VARIABLE-SYNTAX
    imports FRAME-SYNTAX
```

Here we define function module specific syntax.
In order to support function calls with expressions such as `foo(a+b, 1-3)` we define:

```{.k .function .syntax}
    syntax Exp ::= Id "(" Exps ")"
    syntax Exps ::= List{Exp, ","}
```

Further we define function declaration as:

```{.k .function .syntax}
    syntax FunctionDeclaration ::= Type Id "(" Params ")" "{" Stmt "}"
```

With function parameters defined as:

```{.k .function .syntax}
    syntax Param ::= Type Id
    syntax Params ::= List{Param, ","}
```

We add define return statement syntax:

```{.k .function .syntax}
    syntax FunctionStatement ::= "return" ";"
                               | "return" Exp ";"
```

And extend global statements `Stmt` sort with function specific sorts:

```{.k .function .syntax}
    syntax Stmt ::= FunctionDeclaration
                  | FunctionStatement

endmodule
```

## Private syntax

Function module uses helpers specific only to the module.
This syntax is for internal use and should not be part of user facing C--.

```{.k .function .syntax .private}
module FUNCTION-PRIVATE-SYNTAX
    imports FUNCTION-SYNTAX
    imports ERROR-SYNTAX

    syntax ParsedFunction ::= "function" "(" Type ";" Params ";" Stmt ")"

    syntax FuncCallStage ::= "evalExpressions"
                           | "reverseExpressions"
                           | "generateAssignments"

    syntax KItem ::= "#setupFrame" "(" FuncCallStage ";" Params ";" Exps ";" Exps ")"
                   | #return(Type)

endmodule
```

## Configuration

```{.k .function .config}
module FUNCTION-CONFIG
    imports DOMAINS

    configuration
        <functionCell>
            <functions> .Map </functions>
        </functionCell>

endmodule
```

## Semantics

```{.k .function .semantics}
module FUNCTION
    imports DOMAINS

    imports FUNCTION-PRIVATE-SYNTAX

    imports FUNCTION-CONFIG
    imports PGM-CONFIG

    // Parse and store function declaration
    rule <k> ReturnType:Type Name:Id (ParameterList:Params) {Statements:Stmt} => . ...</k>
         <functions>
            FS => FS [Name <- function(ReturnType; ParameterList; Statements)]
         </functions>

    // initial function call to frame conversion
    rule <k>
            Name:Id (Arguments:Exps) =>
            #setupFrame(evalExpressions; Parameters:Params; Arguments; .Exps) ~> Body ~> #return(ExpectedType) ~> #delFrame() ...
        </k>
         <functions>
             ...
             Name |-> function(ExpectedType; Parameters; Body)
             ...
         </functions>

    // eval expression values that came from a function call
    context #setupFrame(evalExpressions; _; HOLE:Exp, _; _)

    // eval any remaining expression values in the "old" frame
    context #setupFrame(_; _; HOLE:Exp, _; _)

    // and move the values to the new list
    rule #setupFrame(evalExpressions; _; E:Exp, L1 => L1; L2 => E, L2)
      requires isKResult(E)

    // When done moving values to the new list reverse list of expressions
    rule #setupFrame(evalExpressions => reverseExpressions; _; .Exps; _)
    rule #setupFrame(reverseExpressions; _; L1 => E, L1; E:Exp, L2 => L2)

    // When done reversing, setup frame and generate assignments
    rule #setupFrame(reverseExpressions; Parameters; Arguments; .Exps) =>
         #newFrame() ~> #setupFrame(generateAssignments; Parameters; Arguments; .Exps)

    rule #setupFrame(generateAssignments; TypeValue Name, RemainingParameters; Argument, RemainingArguments; L2 ) =>
         TypeValue Name = Argument; ~> #setupFrame(generateAssignments; RemainingParameters; RemainingArguments; L2)

    // Clean up when no params and expressions are left
    rule #setupFrame(generateAssignments; .Params; .Exps; .Exps ) => .

    // TODO: pass parameters and arguments into the error msg
    rule #setupFrame(_; _Parameters; _Arguments; _) =>
         #error("Number of parameters and arguments don't match") [owise]

    // https://learn.microsoft.com/en-us/cpp/c-language/return-statement-c?view=msvc-170

    rule return ; ~> (_Skip:Stmt => .) ~> #return(_)  [structural]
    rule return _; ~> (_Skip:Stmt => .) ~> #return(_) [structural]

    // Deal with return statements
    context return HOLE:Exp ;

    // Void functions can have empty return statements
    rule return ; ~> #return(void) ~> #delFrame() => #delFrame() ~> void, undefined

    // Non-void functions must hav a return value
    rule return ; ~> #return(_:Type) ~> #delFrame() => #error("Non-void function must return a value") [owise]

    // There is no explicit return statement for void function
    rule #return(void) ~> #delFrame() => #delFrame() ~> void, undefined

    // Stop processing with an error if non-void function has no return value
    rule #return(_:Type) ~> #delFrame() => #error("Non-void function does not return a value") [owise]

    rule return Outbound:Type, V:Value ; ~> #return(ExpectedType) ~> #delFrame() => #delFrame() ~> ExpectedType, V
      requires #canBeAssignedTo(ExpectedType, Outbound)

    // TODO: pass type names into the error msg
    rule return Outbound:Type, _:Value ; ~> #return(Expected:Type) ~> #delFrame() =>
         #error("Return value has an unexpected type")
      requires notBool #canBeAssignedTo(Expected, Outbound)

endmodule
```
