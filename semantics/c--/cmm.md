# C--

Users of this module should be able to import it and expect complete implementation of C--.
This module follows a standard K framework module pattern where it requires other components and imports them inside its own module.
The only difference here is that we don't have any syntax or semantic rules.

## Requirements

We require language components:

```{.k .cmm .requirements}
requires "type.md"
requires "expression/expression.md"
requires "variable.md"
requires "function.md"
requires "frame.md"
requires "statement.md"
requires "preprocessing/include.md"
```

Language components need to be aware of the break points that we call steps.
Steps are an interface where other modules like "Linear Temporal Logic" or environment can analyze the current state or change execution behaviour.

```{.k .cmm .requirements}
requires "../common/step.md"
```

Whenever we detect ambiguous or unsupported feature we stop execution with an error.

```{.k .cmm .requirements}
requires "../common/error.md"
```

## Syntax

Inside the "CMM-SYNTAX" module we import syntax of all the components:

```{.k .cmm .syntax}
module CMM-SYNTAX
    imports DOMAINS-SYNTAX
    imports INCLUDE-SYNTAX

    imports TYPE-SYNTAX
    imports EXPRESSION-SYNTAX
    imports VARIABLE-SYNTAX
    imports FUNCTION-SYNTAX
    imports STATEMENT-SYNTAX

endmodule
```

## Configuration

"CMM-CONFIG" module is responsible for relevant configuration:

```{.k .cmm .config}
module CMM-CONFIG
    imports FRAME-CONFIG
    imports FUNCTION-CONFIG

endmodule
```

## Semantics

The semantics module called "CMM" includes all the syntax, configuration, and rules.

```{.k .cmm .semantics}
module CMM
    imports CMM-SYNTAX
    imports CMM-CONFIG

    imports INCLUDE
    imports TYPE
    imports EXPRESSION
    imports VARIABLE
    imports FUNCTION
    imports STATEMENT

endmodule
```
