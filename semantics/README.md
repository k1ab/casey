# About

Semantics section defines language and environment components.
Using K framework and rule definitions we generate an interpreter that takes source code
and runs it in a given environment.

This section contains separate components such as language definitions like [C--](c--/)
or [environment](environment/).
Subsection [top](top/) contains top level definitions that use some or all of the components.

## Semantics navigation

<!--nav-->
* [About](README.md)
* [Common](common/)
* [C--](c--/)
* [Environment](environment/)
* [Hooks](hook/)
* [LTL](ltl/)
* [Top Definitions](top/)
* [Tests and examples](../tests/)
