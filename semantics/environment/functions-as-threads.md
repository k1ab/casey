# Process functions as threads

This modules processes declared functions as functions running in parallel with access to the same global variables.
For example if user defines functions `foo` and `bar` in the same file,
the module will create separate `foo` and `bar` thread states.
The module relies on provided interleaving sequence to run functions in predetermined order.
The interleaving sequence `[foo, bar, foo, bar]` means that the module will run
function `foo` until it encounters a step marker.
After that it will run `bar` function until it encounters a step marker.
The module will follow the interleaving sequence until there are no more items in the sequence.

## Requirements

```{.k .main-cmm .requirements}
requires "../c--/cmm.md"
requires "../common/pgm.md"
requires "../common/step.md"
```

## Syntax

```{.k .functions-as-threads .syntax}
module FUNCTIONS-AS-THREADS-SYNTAX
    imports CMM-SYNTAX
    imports PGM-SYNTAX

endmodule
```

## Configuration

Currently approximate example of threaded configuration:

```xml
<threads>
    <thread>
        <tid> foo </tid>
        <frames>
            <frame>
                <fid> 0 </fid>
                <variables>
                    a |-> int8_t, 1
                </variables>
            </frame>
        </frames>
    </thread>
    <thread>
        <tid> bar </tid>
        <frames>
            <frame>
                <fid> 0 </fid>
                <variables>
                    b |-> int8_t, 2
                </variables>
            </frame>
        </frames>
    </thread>
</threads>

<globals>
    a |-> int8_t, 1
    b |-> int8_t, 2
</globals>
```

```{.k .functions-as-threads .syntax}
module FUNCTIONS-AS-THREADS-CONFIG
    imports CMM-CONFIG
    imports PGM-CONFIG
    imports DOMAINS

    syntax KItem ::= "#stageParse"
                   | "#stageCreateThreads"
                   | "#stageProcessThreads"

    configuration
        <threadCell>
            <threads>
                <thread multiplicity="*" type="Map">
                    <tid> #token("main", "Id") </tid>
                    <k/>
                    <frameCell/>
                </thread>
            </threads>

            <globals>.Map</globals>
            <stage>#stageParse</stage>

            <threadedFunc> $Tfunc:List </threadedFunc>
            <threadSequence> $Tseq:List </threadSequence>
        </threadCell>
endmodule
```

## Semantics

```{.k .functions-as-threads .semantics}
module FUNCTIONS-AS-THREADS
    imports DOMAINS
    imports FUNCTIONS-AS-THREADS-SYNTAX
    imports FUNCTIONS-AS-THREADS-CONFIG
    imports CMM


    // Get global variables and remove the "main" function parsing thread
    rule <threads>
            (<thread>
                <tid> #token("main", "Id") </tid>
                <k> #endParse </k>
                <frameCell>
                    <frames>
                        <frame>
                            <fid> 0 </fid>
                            <variables> Vars </variables>
                        </frame>
                    </frames>
                    <topFrameNum> 0 </topFrameNum>
                </frameCell>
            </thread> => .Bag)
        </threads>
        <globals>.Map => Vars</globals>
        <stage>#stageParse => #stageCreateThreads</stage>
        <threadedFunc> Funcs </threadedFunc>
            requires size(Funcs) >Int 0
    // TODO: stop with and error when we get an empty list of threaded functions

    // Set up threads for processing
    rule <threads>
            (.Bag =>
            <thread>
                <tid> Func </tid>
                <k> #threadWait ~> Func (Arguments) ~> #threadEnd </k>
                <frameCell>
                    <frames>
                        <fid> 0 </fid>
                        <variables> Vars </variables>
                    </frames>
                    <topFrameNum> 0 </topFrameNum>
                </frameCell>
            </thread>
            )
            ...
        </threads>
        <globals>Vars</globals>
        <stage> #stageCreateThreads </stage>
        <threadedFunc>
            ListItem(Func:Id (Arguments:Exps)) Rest => Rest
        </threadedFunc>

    // Start thread processing stage
    rule <stage> #stageCreateThreads => #stageProcessThreads </stage>
         <threadedFunc> .List </threadedFunc>

    // Pick a thread from interleaving list and run it
    rule <threads>
            ...
            <thread>
                <tid>FuncName</tid>
                <k> #step(threadWait) => . ... </k>
                ...
            </thread>
            ...
         </threads>

        <stage>#stageProcessThreads</stage>
        <threadSequence> ListItem(FuncName) Rest => Rest </threadSequence>

endmodule
```
