# About

Environment contains modules that emulate critical environment.

## Section navigation

<!--nav-->
* [About](README.md)
* [Default main function](main.md)
* [Process functions as threads](functions-as-threads.md)
