# Default main function execution

This module is responsible for automatically calling `main` function.

## Requirements

We nee `C--` components and initial program configuration so we import:

```{.k .main .requirements}
requires "../c--/cmm.md"
requires "../common/pgm.md"
```

## Syntax

```{.k .main .syntax}
module MAIN-SYNTAX
    imports DOMAINS-SYNTAX
    imports CMM-SYNTAX
    imports PGM-SYNTAX
```

Declare a special `main` token.

```{.k .main .syntax}
    syntax Id ::= "main" [token]
```

```{.k .main .syntax}
endmodule
```

## Semantics

```{.k .main .semantics}
module MAIN
    imports MAIN-SYNTAX
    imports PGM-CONFIG
    imports FRAME-CONFIG

    rule #endParse => main(.Exps)

    rule <k> _:TypedValue => . </k>
         <fid> 0 </fid>

endmodule
```
