# Printf

Since we don't not need to test library implementations we add a hook to `printf`
implemented in C.

At this point this is very restricted version of `printf`.
It works only with exactly one string followed by exactly one expression.

## Requirements

We need types in order to evaluate expressions to TypedValue.
This way our `printf` function works with variable lookup and expressions.

```{.k .printf .requirements}
requires "../c--/type.md"
```

## Syntax

```{.k .printf .syntax}
module PRINTF-SYNTAX
    imports DOMAINS
    imports TYPE-SYNTAX

    syntax Exp
    syntax Printer ::= "printf" "(" String "," Exp ")" ";"

    syntax KItem ::= #print(String, TypedValue)
                   | #printed(Int)

    syntax Int ::= #print(String, Int) [function, hook(IOLIB.printf)]

endmodule
```

## Semantics

```{.k .printf .semantics}
module PRINTF
    imports PRINTF-SYNTAX
    imports TYPE

    context printf(_:String, HOLE:Exp);

    rule printf(S:String, E:Exp) ; => #print( S, {E}:>TypedValue )
      requires isKResult(E)

    rule #print(S, _T, V:Int) => #printed(#print(S, V))

    rule #printed(_) => .

endmodule
```

## Compilation step

In order to enable this hook, definition that includes this module needs to be compiled with special flags:

```bash
kompile --hook-namespaces IOLIB -ccopt ./iolib.cpp printf.md
```
