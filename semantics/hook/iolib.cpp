
#include "/usr/include/kllvm/runtime/header.h"
#include <cstdio>

extern "C"
{

    SortInt hook_IOLIB_printf(SortString format, SortInt x)
    {
        int arg = mpz_get_si(x);
        mpz_t result;
        mpz_init_set_si(result, printf(format->data, arg));
        return move_int(result);
    }
}
