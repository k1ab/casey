# Thread sync based on Peterson's algorithm

Here we describe what the project is doing based on a simple concurrent programming algorithm example for mutual exclusion.

## Our goals

* We want to ensure protocol properties for concurrently executed code.
* Automatically validate provided properties when the code or property definition changes.

## Problem example

A common problem in multithreaded programming is synchronizing reads and writes from multiple threads.
There are to parts in writing a successful solution.
One is to express the problem in terms of a language (in our case it is C).
The other part is getting the logic right.
In this example we separate those parts into `main.c` and a protocol.

### Main

```C
--8<-- "tests/semantics/example/peterson/main.c"
```

This file contains language constructs that setup the environment.
There is also some logic to infer what happened with the threads.
All the "brains" are in a protocol.

### Simple increment protocol

Let's start with an example where each thread will simply add 1 to the result:

```C
--8<-- "tests/semantics/example/peterson/protocol_result_inc.h"
```

This works fine. We use it in such a way that we reach the goal very fast and both treads terminate.

### Critical section protocol

Now if we have to do something "dangerous" when accessing the shared memory the process might never terminate.

```C
--8<-- "tests/semantics/example/peterson/protocol_critical_section.h"
```

Let's kompile the code (don't forget to uncomment the right include line):

```example
gcc -pthread tests/semantics/example/main.c -o tests/semantics/example/bin/peterson
```

Run the program several times and note the difference:

```example
tests/semantics/example/bin/peterson
```

It always produces different values and some times it doesn't even terminate.

### Peterson's algorithm protocol

We obviously need a way to allow only one thread to work on the critical section.
One famous way to allow multiple threads to use single memory resource without conflicts is [Peterson's algorithm](https://en.wikipedia.org/wiki/Peterson%27s_algorithm).

```C
--8<-- "tests/semantics/example/peterson/protocol_peterson.h"
```

It can be compiled and executed the same way as the previous example.

## Compile Casey interpreter for multi-threaded functions

In order to verify this protocol we'll use a [multi-threaded-cmm.md](../../semantics/top/multi-threaded-cmm.md)
collection of rules that relies on defining [functions as threads](../../semantics/environment/functions-as-threads.md).
This means that we assume function defined in our protocol run as separate threads.

First we need to build the interpreter that will run `C--` source code in multithreaded environment:

```bash
kompile semantics/top/multi-threaded-cmm.md
```

## Checking a single possible execution path

When we run the interpreter, we need to specify which functions in the source code should be assumed to run in different threads.
We use `-cTfunc` parameter for that.
We also specify `-cTseq` parameter that sets specific interleaving for those functions.

```bash
krun -cTfunc='ListItem(foo()) ListItem(bar())' -cTseq='ListItem(bar) ListItem(foo)' tests/semantics/example/peterson/protocol_peterson.h
```

<!--pytest-codeblocks:expected-output-->
```xml
```

## Using Casey to analyze the protocol

<!--TODO: Add this section. -->

## Working with LTL formula

<!-- in a separate config files, inside comments near the code, in the code itself. -->
<!-- TODO: describe where these properties are supposed to be stored: -->

Testing this kind of code is difficult even for such a simple case.
Instead of writing tests for all possible combinations,
we make can make sure that certain important properties true for all possible thread combinations.

<!--TODO: Add formula examples. -->

## Cleanup

```example
rm -r multi-threaded-cmm-kompiled
```

## Plugging verification into a continuous integration (CI) process

Note that this is still work-in-progress but here is the goal:

Changed files and desired code properties (described by LTL formula) are uploaded to the verification server via an API.
The server builds a state tree and verifies the code properties hold for all states.
If errors are found, they will be sent back to the caller together with the sequence path where it breaks.
