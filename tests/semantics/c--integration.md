# C-- integration tests

Here we test how all the `C--` components fit together.
In order to run all those tests from project root directory call:

```example
pytest --codeblocks tests/semantics/c--integration.md
```

This file can be used as a comprehensive example of what is supported in `C--` and
how to generate and use `C--` interpreter.

## Compile

For testing `C--` components we need a single threaded environment that behaves
very closely to how compiled `C` code would work if it was compiled by `GCC` or `Clang`.

We pick existing [main-cmm](../../semantics/top/main-cmm.md) definition and compile it:

```bash
kompile --hook-namespaces IOLIB -ccopt semantics/hook/iolib.cpp semantics/top/main-cmm.md
```

This generates a folder with kompiled definitions that will be used by `krun` command.

## Basic function

We start with a simple main function that calls `foo`.

```C
--8<-- "tests/semantics/example/integration/function.c"
```

Here we check several semantic properties:

* Function declaration
* Function call
* Return undefined value when using `return;` ***
* Return value when using `return val;`
* Statements after `return` do not have an effect
* Execute default `main` function
* Support void type
* Support void functions without return

```bash
krun tests/semantics/example/integration/function.c --output none
```

We expect to get:
<!--pytest-codeblocks:expected-output-->
```text
Void function with empty return: a == 8
Void function with no return: b == 9
Non-void function return result == 16
done  0
```

Now we compile the same file using Clang:

```bash
clang -o tests/semantics/example/bin/test_function.bin tests/semantics/example/integration/function.c
```

We execute the resulting binary:

```bash
./tests/semantics/example/bin/test_function.bin
```

Validate that we got the same value as when we use our generated interpreter:

<!--pytest-codeblocks:expected-output-->
```text
Void function with empty return: a == 8
Void function with no return: b == 9
Non-void function return result == 16
done  0
```

## Arithmetic

Test arithmetic expressions.

```C
--8<-- "tests/semantics/example/integration/arithmetic.c"
```

```bash
krun tests/semantics/example/integration/arithmetic.c --output none
```

We expect to get:
<!--pytest-codeblocks:expected-output-->
```text
 6 == 6
 2 == 2
 2 == 2
10 == 10
 0 == 0
40 == 40
 2 == 2
 1 == 1
 0 == 0
 0 == 0
 2 == 2
 9 == 9
11 == 11
 1 == 1
```

Now we compile the same file using Clang:

```bash
clang -o tests/semantics/example/bin/test_arithmetic.bin tests/semantics/example/integration/arithmetic.c
```

We execute the resulting binary:

```bash
./tests/semantics/example/bin/test_arithmetic.bin
```

Validate that we got the same value as when we use our generated interpreter:
<!--pytest-codeblocks:expected-output-->
```text
 6 == 6
 2 == 2
 2 == 2
10 == 10
 0 == 0
40 == 40
 2 == 2
 1 == 1
 0 == 0
 0 == 0
 2 == 2
 9 == 9
11 == 11
 1 == 1
```

## Casting and integer overflow

In terms of integer overflow Casey is a lot stricter than GCC or Clang.

If there is a possibility of an integer overflow one must explicitly specify the type of the constant.

For example:

```C
--8<-- "tests/semantics/example/integration/int_overflow_cast.c"
```

When the file is executed by our interpreter:

```bash
krun tests/semantics/example/integration/int_overflow_cast.c --output none
```

It produces the following outcome:
<!--pytest-codeblocks:expected-output-->
```text
128 == 128
32768 == 32768
2147483648 == 2147483648
```

Now we compile the same file using Clang:

```bash
clang -o tests/semantics/example/bin/test_int_overflow_cast.bin tests/semantics/example/integration/int_overflow_cast.c
```

We execute the resulting binary:

```bash
./tests/semantics/example/bin/test_int_overflow_cast.bin
```

Validate that we got the same value as when we use our generated interpreter:
<!--pytest-codeblocks:expected-output-->
```text
128 == 128
32768 == 32768
2147483648 == 2147483648
```

But if we try to assign the maximum 32 bit value with overflow to 64 bit int without the explicit cast:

```C
--8<-- "tests/semantics/example/error/int_overflow_error.c"
```

```bash
krun tests/semantics/example/error/int_overflow_error.c
```

We get an error:

<!--pytest-codeblocks:expected-output-->
```xml
<generatedTop>
  <k>
    #error ( "Integer overflow" , .ErrorMetaList ) ~> #freezer_;_OPERATOR-SYNTAX_Stmt_AssignmentExpression1_ ( f64 ~> . ) ~> return 0 ; ~> #return ( int32_t ) ~> #delFrame ( ) ~> .
  </k>
  <frameCell>
    <frames>
      <frame>
        <fid>
          0
        </fid>
        <variables>
          f64 |-> int64_t , undefined
        </variables>
      </frame> <frame>
        <fid>
          1
        </fid>
        <variables>
          .Map
        </variables>
      </frame>
    </frames>
    <topFrameNum>
      1
    </topFrameNum>
  </frameCell>
  <functionCell>
    <functions>
      main |-> function ( int32_t ; .Params ; f64 = 2147483647 + 1 ; return 0 ; )
    </functions>
  </functionCell>
<generatedCounter>
  0
</generatedCounter>
```

## Boolean operators

Casey supports boolean operators as shown in the boolean example:

```C
--8<-- "tests/semantics/example/integration/boolean.c"
```

Running the interpreter:

```bash
krun tests/semantics/example/integration/boolean.c --output none
```

Produces the following result:
<!--pytest-codeblocks:expected-output-->
```text
13 == 13
 0 ==  0
 0 ==  0
 1 ==  1
```

Now we compile the same file using Clang:

```bash
clang -o tests/semantics/example/bin/test_boolean.bin tests/semantics/example/integration/boolean.c
```

We execute the resulting binary:

```bash
./tests/semantics/example/bin/test_boolean.bin
```

Validate that we got the same value as when we use our generated interpreter:
<!--pytest-codeblocks:expected-output-->
```text
13 == 13
 0 ==  0
 0 ==  0
 1 ==  1
```

## Control statements

Support for control statements is shown in the following example:

```C
--8<-- "tests/semantics/example/integration/control.c"
```

Running the interpreter:

```bash
krun tests/semantics/example/integration/control.c --output none
```

Produces the following results:
<!--pytest-codeblocks:expected-output-->
```text
N  == 5
if_var == 5
else_var == 5
while_index == 5
do_index == 5
for_index  == 25
55 == 55
5  == 5
20 == 20
15 == 15
15 == 15
5  == 5
55 == 55
5  == 5
80 == 80
55 == 55
100 == 100
31 == 31
100 == 100
30 == 30
480 == 480
479 == 479
16 == 16
16 == 16
16 == 16
```

Now we compile the same file using Clang:

```bash
clang -o tests/semantics/example/bin/test_control.bin tests/semantics/example/integration/control.c
```

We execute the resulting binary:

```bash
./tests/semantics/example/bin/test_control.bin
```

Validate that we got the same value as when we use our generated interpreter:
<!--pytest-codeblocks:expected-output-->
```text
N  == 5
if_var == 5
else_var == 5
while_index == 5
do_index == 5
for_index  == 25
55 == 55
5  == 5
20 == 20
15 == 15
15 == 15
5  == 5
55 == 55
5  == 5
80 == 80
55 == 55
100 == 100
31 == 31
100 == 100
30 == 30
480 == 480
479 == 479
16 == 16
16 == 16
16 == 16
```

Specialized tests explore separate constructs in greater depth:

```C
--8<-- "tests/semantics/example/integration/while.c"
--8<-- "tests/semantics/example/integration/for.c"
--8<-- "tests/semantics/example/integration/do.c"
--8<-- "tests/semantics/example/integration/break.c"
--8<-- "tests/semantics/example/integration/continue.c"
```

Running these in both the interpreter and Clang produces the following output:

```bash
krun tests/semantics/example/integration/while.c --output none
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (x1)
15 == 15 (y1)
15 == 15 (z1)
89 == 89 (x2)
59 == 59 (y2)
32 == 32 (x3)
91 == 91 (y3)
```

```bash
clang -o tests/semantics/example/bin/test_while.bin tests/semantics/example/integration/while.c
./tests/semantics/example/bin/test_while.bin
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (x1)
15 == 15 (y1)
15 == 15 (z1)
89 == 89 (x2)
59 == 59 (y2)
32 == 32 (x3)
91 == 91 (y3)
```


```bash
krun tests/semantics/example/integration/for.c --output none
```
<!--pytest-codeblocks:expected-output-->
```text
 5 == 5
55 == 55
 5 == 5
```
```bash
clang -o tests/semantics/example/bin/test_for.bin tests/semantics/example/integration/for.c
./tests/semantics/example/bin/test_for.bin
```
<!--pytest-codeblocks:expected-output-->
```text
 5 == 5
55 == 55
 5 == 5
```


```bash
krun tests/semantics/example/integration/do.c --output none
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (x)
5 == 5 (y)
6 == 6 (x1)
5 == 5 (y1)
10 == 10 (x2)
29 == 29 (y2)
```
```bash
clang -o tests/semantics/example/bin/test_do.bin tests/semantics/example/integration/do.c
./tests/semantics/example/bin/test_do.bin
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (x)
5 == 5 (y)
6 == 6 (x1)
5 == 5 (y1)
10 == 10 (x2)
29 == 29 (y2)
```


```bash
krun tests/semantics/example/integration/break.c --output none
```
<!--pytest-codeblocks:expected-output-->
```text
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
25 == 25 (x0)
41 == 41 (y0)
16 == 16 (y1)
16 == 16 (z1)
25 == 25 (x0n)
45 == 45 (y0n)
20 == 20 (z1n)
20 == 20 (z1n)
20 == 20 (x02)
20 == 20 (z02)
20 == 20 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
20 == 20 (x2n)
40 == 40 (y2n)
21 == 21 (x03)
18 == 18 (x3)
18 == 18 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
221 == 221 (y0)
204 == 204 (y1)
255  == 255 (y0n)
238  == 238 (y1n)
20 == 20 (x02)
20 == 20 (z02)
340 == 340 (x2)
340 == 340 (y2)
320 == 320 (z2)
20  == 20 (x02n)
20  == 20 (z02n)
400  == 400 (x2n)
420  == 420 (y2n)
400  == 400 (z2n)
360  == 360 (x03)
340 == 340 (x3)
340 == 340 (y3)
420  == 420 (x03n)
400  == 400 (x3n)
400  == 400 (y3n)
25 == 25 (x0)
41 == 41 (y0)
16 == 16 (y1)
16 == 16 (z1)
25 == 25 (x0n)
69 == 69 (y0n)
44 == 44 (z1n)
44 == 44 (z1n)
20 == 20 (x02)
20 == 20 (z02)
35 == 35 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
39 == 39 (x2n)
59 == 59 (y2n)
21 == 21 (x03)
18 == 18 (x3)
18 == 18 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
```
```bash
clang -o tests/semantics/example/bin/test_break.bin tests/semantics/example/integration/break.c
./tests/semantics/example/bin/test_break.bin
```
<!--pytest-codeblocks:expected-output-->
```text
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
16 == 16 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
16 == 16 (x3)
16 == 16 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
25 == 25 (x0)
41 == 41 (y0)
16 == 16 (y1)
16 == 16 (z1)
25 == 25 (x0n)
45 == 45 (y0n)
20 == 20 (z1n)
20 == 20 (z1n)
20 == 20 (x02)
20 == 20 (z02)
20 == 20 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
20 == 20 (x2n)
40 == 40 (y2n)
21 == 21 (x03)
18 == 18 (x3)
18 == 18 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
221 == 221 (y0)
204 == 204 (y1)
255  == 255 (y0n)
238  == 238 (y1n)
20 == 20 (x02)
20 == 20 (z02)
340 == 340 (x2)
340 == 340 (y2)
320 == 320 (z2)
20  == 20 (x02n)
20  == 20 (z02n)
400  == 400 (x2n)
420  == 420 (y2n)
400  == 400 (z2n)
360  == 360 (x03)
340 == 340 (x3)
340 == 340 (y3)
420  == 420 (x03n)
400  == 400 (x3n)
400  == 400 (y3n)
25 == 25 (x0)
41 == 41 (y0)
16 == 16 (y1)
16 == 16 (z1)
25 == 25 (x0n)
69 == 69 (y0n)
44 == 44 (z1n)
44 == 44 (z1n)
20 == 20 (x02)
20 == 20 (z02)
35 == 35 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
39 == 39 (x2n)
59 == 59 (y2n)
21 == 21 (x03)
18 == 18 (x3)
18 == 18 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
```


```bash
krun tests/semantics/example/integration/continue.c --output none
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
25 == 25 (x0n)
45 == 45 (y0n)
20 == 20 (z1n)
20 == 20 (z1n)
20 == 20 (x02)
20 == 20 (z02)
20 == 20 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
20 == 20 (x2n)
40 == 40 (y2n)
21 == 21 (x03)
20 == 20 (x3)
20 == 20 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
221 == 221 (y0)
204 == 204 (y1)
255  == 255 (y0n)
238  == 238 (y1n)
20 == 20 (x02)
20 == 20 (z02)
400 == 400 (x2)
340 == 340 (y2)
320 == 320 (z2)
20  == 20 (x02n)
20  == 20 (z02n)
400  == 400 (x2n)
420  == 420 (y2n)
400  == 400 (z2n)
420  == 420 (x03)
400 == 400 (x3)
400 == 400 (y3)
420  == 420 (x03n)
400  == 400 (x3n)
400  == 400 (y3n)
25 == 25 (x0n)
69 == 69 (y0n)
44 == 44 (z1n)
44 == 44 (z1n)
20 == 20 (x02)
20 == 20 (z02)
39 == 39 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
39 == 39 (x2n)
59 == 59 (y2n)
21 == 21 (x03)
20 == 20 (x3)
20 == 20 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
```
```bash
clang -o tests/semantics/example/bin/test_continue.bin tests/semantics/example/integration/continue.c
./tests/semantics/example/bin/test_continue.bin
```
<!--pytest-codeblocks:expected-output-->
```text
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
16 == 16 (y1)
16 == 16 (z1)
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
20 == 20 (y1n)
20 == 20 (z1n)
20 == 20 (x2)
15 == 15 (y2)
15 == 15 (z2)
20 == 20 (x2n)
20 == 20 (y2n)
20 == 20 (z2n)
20 == 20 (x3)
20 == 20 (y3)
20 == 20 (x3n)
20 == 20 (y3n)
25 == 25 (x0n)
45 == 45 (y0n)
20 == 20 (z1n)
20 == 20 (z1n)
20 == 20 (x02)
20 == 20 (z02)
20 == 20 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
20 == 20 (x2n)
40 == 40 (y2n)
21 == 21 (x03)
20 == 20 (x3)
20 == 20 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
221 == 221 (y0)
204 == 204 (y1)
255  == 255 (y0n)
238  == 238 (y1n)
20 == 20 (x02)
20 == 20 (z02)
400 == 400 (x2)
340 == 340 (y2)
320 == 320 (z2)
20  == 20 (x02n)
20  == 20 (z02n)
400  == 400 (x2n)
420  == 420 (y2n)
400  == 400 (z2n)
420  == 420 (x03)
400 == 400 (x3)
400 == 400 (y3)
420  == 420 (x03n)
400  == 400 (x3n)
400  == 400 (y3n)
25 == 25 (x0n)
69 == 69 (y0n)
44 == 44 (z1n)
44 == 44 (z1n)
20 == 20 (x02)
20 == 20 (z02)
39 == 39 (x2)
35 == 35 (y2)
20  == 20 (x02n)
20  == 20 (z02n)
39 == 39 (x2n)
59 == 59 (y2n)
21 == 21 (x03)
20 == 20 (x3)
20 == 20 (y3)
20  == 20 (x03n)
20 == 20 (x3n)
19 == 19 (y3n)
```


## Cleanup

Now we can remove temporary folders built by the [Compile](#compile) step.

```bash
rm -r main-cmm-kompiled
rm tests/semantics/example/bin/test_*.bin
```
