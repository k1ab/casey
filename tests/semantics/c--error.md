# C-- error tests

Some language constructs explicitly generate an error.

In order to run all those tests from project root directory call:

```example
pytest --codeblocks tests/semantics/c--error.md
```

This file can be used as a comprehensive example of what is NOT supported in `C--`.

## Compile

We pick existing single-threaded [main-cmm](../../semantics/top/main-cmm.md) definition and compile it:

```bash
kompile --hook-namespaces IOLIB -ccopt semantics/hook/iolib.cpp semantics/top/main-cmm.md
```

This generates a folder with kompiled definitions that will be used by `krun` command.

## Non-void function does not return a value

```C
--8<-- "tests/semantics/example/error/no_return_stmt.c"
```

`Clang` compiles the above file with a warning "non-void function does not return a value".

```bash
clang -o tests/semantics/example/bin/test_no_return_stmt.bin tests/semantics/example/error/no_return_stmt.c
```

When we execute the binary

```bash
./tests/semantics/example/bin/test_no_return_stmt.bin
```

we get:

<!--pytest-codeblocks:expected-output-->
```text
func_return == 0
```

In `C--` non-void functions that do not have a return value produce an error:

```bash
krun tests/semantics/example/error/no_return_stmt.c
```

<!--pytest-codeblocks:expected-output-->
```xml
<generatedTop>
  <k>
    #error ( "Non-void function does not return a value" , .ErrorMetaList ) ~> #freezer_;_OPERATOR-SYNTAX_Stmt_AssignmentExpression1_ ( res ~> . ) ~> printf ( "func_return == %d\n" , res ) ; return 0 ; ~> #return ( int32_t ) ~> #delFrame ( ) ~> .
  </k>
  <frameCell>
    <frames>
      <frame>
        <fid>
          0
        </fid>
        <variables>
          a |-> int32_t , 8
          b |-> int32_t , 9
        </variables>
      </frame> <frame>
        <fid>
          1
        </fid>
        <variables>
          res |-> int32_t , undefined
        </variables>
      </frame> <frame>
        <fid>
          2
        </fid>
        <variables>
          p |-> int32_t , 2
          x |-> int32_t , 16
        </variables>
      </frame>
    </frames>
    <topFrameNum>
      2
    </topFrameNum>
  </frameCell>
  <functionCell>
    <functions>
      main |-> function ( int32_t ; .Params ; int32_t res ; res = no_return_stmt ( 2 , .Exps ) ; printf ( "func_return == %d\n" , res ) ; return 0 ; )
      no_return_stmt |-> function ( int32_t ; int32_t p , .Params ; int32_t x = a * p ; )
    </functions>
  </functionCell>
<generatedCounter>
  0
</generatedCounter>
```

## Non-void function has empty return

```C
--8<-- "tests/semantics/example/error/non_void_empty_return.c"
```

Here `Clang` will not compile and return an return and error
"error: non-void function 'non_void_empty_return' should return a value".

In `C--` we also generate an error in this case:

```bash
krun tests/semantics/example/error/non_void_empty_return.c
```

<!--pytest-codeblocks:expected-output-->
```xml
<generatedTop>
  <k>
    #error ( "Non-void function must return a value" , .ErrorMetaList ) ~> #freezer_;_OPERATOR-SYNTAX_Stmt_AssignmentExpression1_ ( res ~> . ) ~> printf ( "func_return == %d\n" , res ) ; return 0 ; ~> #return ( int32_t ) ~> #delFrame ( ) ~> .
  </k>
  <frameCell>
    <frames>
      <frame>
        <fid>
          0
        </fid>
        <variables>
          a |-> int32_t , 8
          b |-> int32_t , 9
        </variables>
      </frame> <frame>
        <fid>
          1
        </fid>
        <variables>
          res |-> int32_t , undefined
        </variables>
      </frame> <frame>
        <fid>
          2
        </fid>
        <variables>
          p |-> int32_t , 2
          x |-> int32_t , 16
        </variables>
      </frame>
    </frames>
    <topFrameNum>
      2
    </topFrameNum>
  </frameCell>
  <functionCell>
    <functions>
      main |-> function ( int32_t ; .Params ; int32_t res ; res = non_void_empty_return ( 2 , .Exps ) ; printf ( "func_return == %d\n" , res ) ; return 0 ; )
      non_void_empty_return |-> function ( int32_t ; int32_t p , .Params ; int32_t x = a * p ; return ; )
    </functions>
  </functionCell>
<generatedCounter>
  0
</generatedCounter>
```

## Effectless statement

```C
--8<-- "tests/semantics/example/error/expression_result_unused.c"
```

`Clang` compiles the above file with a warning "expression result is unused".

```bash
clang -o tests/semantics/example/bin/expression_result_unused.bin tests/semantics/example/error/expression_result_unused.c
```

The resulting binary runs

```bash
./tests/semantics/example/bin/expression_result_unused.bin
```

as expected:

<!--pytest-codeblocks:expected-output-->
```text
print 0
```

However our C-- interpreter will raise an error:

```bash
krun tests/semantics/example/error/expression_result_unused.c
```

<!--pytest-codeblocks:expected-output-->
```xml
<generatedTop>
  <k>
    #error ( "Expression result unused" , .ErrorMetaList ) ~> printf ( "print %d\n" , 0 ) ; return 0 ; ~> #return ( int32_t ) ~> #delFrame ( ) ~> .
  </k>
  <frameCell>
    <frames>
      <frame>
        <fid>
          0
        </fid>
        <variables>
          .Map
        </variables>
      </frame> <frame>
        <fid>
          1
        </fid>
        <variables>
          .Map
        </variables>
      </frame>
    </frames>
    <topFrameNum>
      1
    </topFrameNum>
  </frameCell>
  <functionCell>
    <functions>
      main |-> function ( int32_t ; .Params ; 3 + 3 ; printf ( "print %d\n" , 0 ) ; return 0 ; )
    </functions>
  </functionCell>
<generatedCounter>
  0
</generatedCounter>
```

## Cleanup

Now we can remove temporary folders built by the [Compile](#compile) step.

```bash
rm -r main-cmm-kompiled
rm tests/semantics/example/bin/test_*.bin
```
