// This is a simple C code that outputs 8 bit integer 10.
#include <stdio.h>
#include <stdint.h>


int8_t a = (int8_t)10 + (int8_t)5;


int8_t main() {
    printf("%d\n", a);
    return 0;
}
