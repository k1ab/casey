
#include <stdio.h>
#include <stdint.h>


int main() {
    // This statement is okay for C but it raises an error in C--
    3 + 3;

    printf("print %d\n", 0);
    return 0;
}
