#include <stdio.h>
#include <stdint.h>

int64_t f64;

int main() {
    // gcc warning, result -2147483647
    // clang warning, result -2147483647
    // casey, result error
    f64 = 2147483647 + 1;

    //printf("f64 = %ld \n", f64);
    return 0;
}
