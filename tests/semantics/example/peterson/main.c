#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

//#include "protocol_result_inc.h"
//#include "protocol_critical_section.h"
#include "protocol_peterson.h"

typedef struct thread_data {
   int64_t max;
   int64_t counter;
   int64_t iterations;
   int8_t tid;

} thread_data;


void *worker(void *arg)
{

    thread_data *tdata=(thread_data *)arg;

    switch (tdata->tid)
    {
        case 0:
            while (result < tdata->max ) {
                tdata->iterations++;
                tdata->counter += foo();
            };
            break;
        case 1:
            while (result < tdata->max ) {
                tdata->iterations++;
                tdata->counter += bar();
            };
            break;
        default:
	        printf("Unexpected tid=%d!!!\n", tdata->tid);

    }
}

int main() {
    pthread_t p1, p2;
    thread_data foo, bar;

    int64_t max = 10000;

    foo.tid = 0;
    foo.counter = 0;
    foo.iterations = 0;
    foo.max = max;

    bar.tid = 1;
    bar.counter = 0;
    bar.iterations = 0;
    bar.max = max;

    // Start threads
	pthread_create(&p1, NULL, &worker, (void *) & foo);
	pthread_create(&p2, NULL, &worker, (void *) & bar);

	// Wait for the threads to end.
	pthread_join(p1, NULL);
	pthread_join(p2, NULL);

    printf("Global var result is: %ld \n", result);
    printf("\n");
    printf("Foo: counter = %ld, iterations = %ld, iterations - counter = %ld \n", foo.counter, foo.iterations, foo.iterations - foo.counter);
    printf("Bar: counter = %ld, iterations = %ld, iterations - counter = %ld \n", bar.counter, bar.iterations, bar.iterations - bar.counter);
    printf("\n");
    printf("Foo + Bar: counter = %ld, iterations = %ld \n", foo.counter + bar.counter, foo.iterations + bar.iterations);
    printf("result - (foo.counter + bar.counter) = %ld \n", result - (foo.counter + bar.counter));
    return 0;
}
