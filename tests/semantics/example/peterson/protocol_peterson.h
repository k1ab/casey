#include <stdint.h>

int8_t foo_flag = 0;
int8_t bar_flag = 0;
int8_t turn;

int64_t sum = 1;
int64_t result = 1;


int8_t foo(){

    // Foo is waiting to do the work
    foo_flag = 1;

    // Foo's turn is always 1
    turn = 1;

    // If Bar is not waiting to do the work and it is Foo's turn than go
    int8_t go = (bar_flag == 0) == (turn == 1);
    if (go)
    { // critical section

        int64_t tmp = sum;
        sum = -10000000;

        tmp = tmp + 1;
        sum = tmp;

        result = sum;

        //end of critical section

        // let someone else work on the critical section
        foo_flag = 0;

    }
    return go;
}


int8_t bar(){

    // Bar is waiting to do the work
    bar_flag = 1;

    // Bar's turn is always 0
    turn = 0;

    // if foo is not waiting to do the work and it is Bar's turn than go
    int8_t go = (foo_flag == 0) == (turn == 0);
    if (go)
    { // critical section
        int64_t tmp = sum;
        sum = -10000000;

        tmp = tmp + 1;
        sum = tmp;

        result = sum;

        // end of critical section

        // let someone else work on the critical section
        bar_flag = 0;
    }
    return go;
}
