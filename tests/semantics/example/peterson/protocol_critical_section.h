#include <stdint.h>


int64_t sum = 1;
int64_t result = 1;


int8_t foo(){
    // critical section
    int64_t tmp = sum;
    sum = -10000000;

    tmp = tmp + 1;
    sum = tmp;

    result = sum;
    // end of critical section

    return 1;
}


int8_t bar(){
    // critical section
    int64_t tmp = sum;
    sum = -10000000;

    tmp = tmp + 1;
    sum = tmp;

    result = sum;
    // end of critical section

    return 1;
}
