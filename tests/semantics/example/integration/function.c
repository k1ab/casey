#include <stdio.h>
#include <stdint.h>

int a = 8;
int b = 9;


void with_empty_return(){
    printf("Void function with empty return: a == %d\n", a);
    return;
    printf("Should not be part of output %d\n", a);
}


void no_return(){
    printf("Void function with no return: b == %d\n", b);
}

int int_return(int p){
    return a * p;
    printf("should not be part of output %d\n", p);
}

int main(){
    with_empty_return();
    no_return();

    int res;
    res = int_return(2);
    printf("Non-void function return result == %d\n", res);

    printf("done  %d\n", 0);
    return 0;
}
