#include <stdio.h>
#include <stdint.h>


int main() {

    int level1 = 0;
    int level2 = 0;
    int level1_2 = 0;

    for(int i = 0; i < 5; i = i + 1) {
        // max 5
        level1 = level1 + 1;
        for(int j = 0; j < 20; j = j + 1) {
            if (j > 10) {
                continue;
            }
            // max 10
            level2 = level2 + 1;
        }
        level1_2 = level1_2 + 1;
        if(level1_2 > 4)
        {
            continue;
        }
    }

    printf(" 5 == %d\n", level1);
    printf("55 == %d\n", level2);
    printf(" 5 == %d\n", level1_2);

    return 0;
}
