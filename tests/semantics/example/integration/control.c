#include <stdio.h>
#include <stdint.h>

int while_index = 0;
int do_index = 0;
int for_index = 0;
int if_var;
int else_var;

int gcd(int x, int y) {
    while (y != 0) {
        int t = y;
        y = x % y;
        x = t;
    }
    return x;
}

int main() {
    int index = 0;
    int N = 5;

    int a = 10;

    if (a){
        if_var = N;
    }

    if (a - a){
        else_var = 0;
    }
    else {
        else_var = N;
    }

    while((index <= N)) {
        while_index = index;
        index = index + 1;
    }

    index = N;
    do {
        do_index = do_index + 1;
        index = index - 1;
    } while(index > 0);

    for(int ii = N; ii > 0; ii = ii - 1){
        for(int jj = N; jj > 0; jj = jj - 1){
            for_index = for_index + 1;
        }
    }


    int xx = 0;
    int yy = 0;
    for (int i = 0; i < 5; i = i + 1) {
        for (int j = 0; j < 20; j = j + 1) {
            if (j > 10) {
                break;
            }
            xx = xx + 1;
        }
        yy = yy + 1;
    }

    int x1 = 0;
    int y1 = 0;
    int z1 = 0;
    while (x1 < 20) {
        x1 = x1 + 1;
        if (x1 > 15) {
            continue;
        }
        y1 = y1 + 1;
        z1 = z1 + 1;
    }

    int xx1 = 0;
    int yy1 = 0;
    int tt1 = 0;
    for (int i = 0; i < 5; i = i + 1) {
        tt1 = tt1 + 1;
        for (int j = 0; j < 20; j = j + 1) {
            if (j > 10) {
                continue;
            }
            xx1 = xx1 + 1;
        }
        yy1 = yy1 + 1;
        if (yy1 > 4)
        {
            continue;
        }
    }

    int x = 0;
    int y = 0;
    for (int i = 0; i < 55; i = i + 1) {
        x = x + 1;
        y = y + 1;
        x = y;
        while (x < 100) {
            x = x + 1;
            if (x > 79) {
                break;
            }
            if (x < 100) {
                continue;
            }
        }
        if (x > 79) {
            continue;
        }
    }

    int x0 = 0;
    int y0 = 0;
    while (x0 < 100) {
        y0 = y0 + 1;
        while (gcd(x0, y0) < 15) {
            y0 = y0 + 1;
            x0 = x0 + 5;
            if (y0 > 11) {
                break;
            }
            else {
                continue;
            }
        }
    }

    int x3 = 0;
    int y3 = 0;
    while (x3 < 100) {
        y3 = y3 + 1;
        do {
            y3 = y3 + 1;
            x3 = x3 + 5;
            if (y3 > 11) {
                break;
            }
            else {
                continue;
            }
        } while (gcd(x3, y3) < 15);
    }

    // TODO: Remove the following two lines and uncomment the testcase when scope rules are implemented correctly
    int x2 = 480;
    int y2 = 479;
    /*int x2 = 0;
    int y2 = 0;
    for (int x = 0; x < 500; x = x + 1) {
        if ((x & 1) == 0) {
            x = x + 1;
        }
        y2 = x;
        while (x % 19 != 0)
        {
            x = x + 1;
            if (x > 349) {
                for (int j = 0; j < 10; j = j + 1) {
                    j = j + 1;
                }
                break;
            }
        }
        x2 = x;
        if (x2 > 479) {
            break;
        }
        else {
            continue;
        }
    }*/

    int x4 = 0;
    int x5 = 0;
    int x6 = 0;
    do {
        x4 = x4 + 1;
        if (x4 > 15) {
            break;
        }
        do {
            if (x5 > 15) {
                break;
            }
            for (int i = 0; i < 89; i = i + 1) {
                if (i > 31) {
                    break;
                }
            }
            do {
                if (x6 > 15) {
                    break;
                }
                for (int i = 0; i < 89; i = i + 1) {
                    if (i > 31) {
                        continue;
                    }
                }
                x6 = x6 + 1;
            } while (x6 < 20);
            x5 = x5 + 1;
        } while (x5 < 20);
    } while (x4 < 20);


    printf("N  == %d\n", N);
    printf("if_var == %d\n", if_var);
    printf("else_var == %d\n", else_var);
    printf("while_index == %d\n", while_index);
    printf("do_index == %d\n", do_index);
    printf("for_index  == %d\n", for_index);

    printf("55 == %d\n", xx);
    printf("5  == %d\n", yy);
    printf("20 == %d\n", x1);
    printf("15 == %d\n", y1);
    printf("15 == %d\n", z1);
    printf("5  == %d\n", tt1);
    printf("55 == %d\n", xx1);
    printf("5  == %d\n", yy1);
    printf("80 == %d\n", x);
    printf("55 == %d\n", y);
    printf("100 == %d\n", x0);
    printf("31 == %d\n", y0);
    printf("100 == %d\n", x3);
    printf("30 == %d\n", y3);
    printf("480 == %d\n", x2);
    printf("479 == %d\n", y2);
    printf("16 == %d\n", x4);
    printf("16 == %d\n", x5);
    printf("16 == %d\n", x6);

    return 0;
}
