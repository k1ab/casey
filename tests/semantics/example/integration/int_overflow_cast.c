#include <stdio.h>
#include <stdint.h>

int8_t a8;
int16_t b16;
int16_t c16;
int32_t d32;
int32_t e32;
int64_t f64;
int64_t f64b;
int64_t g64;


int32_t main() {
    // gcc no warning, result -128
    // clang warning, result -128
    // casey, result error
    //a8 = 127 + 1;

    // result 128
    b16 = (int16_t)127 + 1;

    // gcc no warning, result -32768
    // clang warning, result -32768
    // c16 = 32767 + 1;
    // result 32768
    d32 = (int32_t)32767 + 1;

    // gcc warning, result -32768
    // clang warning, result -32768
    // casey, result error
    //e32 = 2147483647 + 1;

    // Produces very different behavior from previous examples!
    // gcc warning, result -2147483647
    // clang warning, result -2147483647
    // casey, result error
    //f64 = 2147483647 + 1;

    // gcc no warning, result 2147483648
    // clang no warning, result 2147483648
    // casey, result 2147483648
    f64b = (int64_t)2147483647 + 1;

    // gcc warning, result -9223372036854775808
    // clang warning, result -9223372036854775808
    // casey, result error
    //g64 = 9223372036854775807 + 1;

    //printf("a8 = %d \n", a8);
    printf("128 == %d\n", b16);
    printf("32768 == %d\n", d32);
    printf("2147483648 == %ld\n", f64b);
    return 0;
}
