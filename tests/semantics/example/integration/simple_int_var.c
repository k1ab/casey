#include <stdio.h>
#include <stdint.h>

int8_t a = 10;
int8_t b;
int8_t c = 20;

int8_t foo() {
    return 100;
}

void bar() {
    b = 30;
}

int16_t main() {
    // Changing global var "a" to the return value of foo()
    a = foo();
    // Check that global "b" was updated
    bar();

    printf("a = %d\n", a);
    printf("b = %d\n", b);

    return 0;
    // This part is skipped and will not have any effect.
    b = 10;
    a = 11;
}
