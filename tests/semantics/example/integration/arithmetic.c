#include <stdio.h>
#include <stdint.h>

int8_t a;
int8_t m;
int8_t d;
int8_t s;
int f;
int l_shift;
int r_shift;
int or;
int and;
int lt;
int gt;
int neq;
int bor;
int bxor;
int band;
int not;


int main(){

    a = 1 + 2 + 3;
    a = a + 2 - (3 - 1);

    m = 2 * 2 * 2;
    m = m / 2 / 2;
    d = 2 / (2 / 2);
    s = (1 + 2) * 3 / 1 + 1;
    f = 2 + 5 % 3 == (2 + 5) % 3;

    l_shift = 10 << 2;
    r_shift = 10 >> 2;

    or = 10 || 0;
    and = 10 && 0;

    lt = 30 < 60;
    gt = 40 > 20;
    neq = 30 <= 30 != 30 >= 30;

    band = 10 & 3;
    bxor = 10 ^ 3;
    bor = 10 | 3;

    not = !neq;

    printf(" 6 == %d\n", a);
    printf(" 2 == %d\n", m);
    printf(" 2 == %d\n", d);
    printf("10 == %d\n", s);
    printf(" 0 == %d\n", f);
    printf("40 == %d\n", l_shift);
    printf(" 2 == %d\n", r_shift);
    printf(" 1 == %d\n", or);
    printf(" 0 == %d\n", and);
    printf(" 0 == %d\n", neq);
    printf(" 2 == %d\n", band);
    printf(" 9 == %d\n", bxor);
    printf("11 == %d\n", bor);
    printf(" 1 == %d\n", not);
    return 0;
}
