#include <stdio.h>
#include <stdint.h>


int main() {
    int x = 0;
    int y = 0;
    do {
        x = x + 1;
        if (x > 5) {
            continue;
        }
        y = y + 1;
        continue;
    } while (x < 20);

    int x1 = 0;
    int y1 = 0;
    do {
        x1 = x1 + 1;
        if (x1 > 5) {
            break;
        }
        y1 = y1 + 1;
        continue;
    } while (x1 < 20);

    int x2 = 0;
    int y2 = 0;
    do {
        x2 = x2 + 1;
        do {
            y2 = y2 + 1;
            if (x2 > 5) {
                break;
            }
        } while (y2 < 20);
    } while (x2 < 10);


    printf("20 == %d (x)\n", x);
    printf("5 == %d (y)\n", y);
    printf("6 == %d (x1)\n", x1);
    printf("5 == %d (y1)\n", y1);
    printf("10 == %d (x2)\n", x2);
    printf("29 == %d (y2)\n", y2);


    return 0;
}