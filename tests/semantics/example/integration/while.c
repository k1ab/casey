#include <stdio.h>
#include <stdint.h>


int main() {
    int x1 = 0;
    int y1 = 0;
    int z1 = 0;
    int x2 = 0;
    int y2 = 0;
    int x3 = 0;
    int y3 = 0;
    while (x1 < 20) {
        x1 = x1 + 1;
        if (x1 > 15) {
            continue;
        }
        while (x2 < 100) {
            x2 = x2 + 1;
            if (x2 > 59) {
                break;
            }
            while (x3 < 42) {
                y3 = y3 + 1;
                if (x3 > 31) {
                    break;
                }
                x3 = x3 + 1;
                if (y3 < 42) {
                    continue;
                }
            }
            y2 = y2 + 1;
            if (x2 < 100) {
                continue;
            }
        }
        y1 = y1 + 1;
        z1 = z1 + 1;
        x2 = x2 + 1;
        if (x1 > 1) {
            continue;
        }
    }

    printf("20 == %d (x1)\n", x1);
    printf("15 == %d (y1)\n", y1);
    printf("15 == %d (z1)\n", z1);
    printf("89 == %d (x2)\n", x2);
    printf("59 == %d (y2)\n", y2);
    printf("32 == %d (x3)\n", x3);
    printf("91 == %d (y3)\n", y3);

    return 0;
}
