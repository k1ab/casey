#include <stdio.h>
#include <stdint.h>

int a;
int b;
int c;
int d;


int main(){

    a = 13;
    b = 0;

    c = a && b;
    d = b || a;

    printf("13 == %d\n", a);
    printf(" 0 ==  %d\n", b);
    printf(" 0 ==  %d\n", c);
    printf(" 1 ==  %d\n", d);
    return 0;
}
