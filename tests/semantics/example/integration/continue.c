#include <stdio.h>
#include <stdint.h>

void test_while()
{
    int y1n = 0;
    int z1n = 0;
    while (y1n < 20) {
        if (y1n > 150) {
            break;
        }
        y1n = y1n + 1;
        z1n = z1n + 1;
    }

    int x2 = 0;
    int y2 = 0;
    int z2 = 0;
    while (x2 < 20) {
        x2 = x2 + 1;
        if (x2 > 15) {
            continue;
        }
        y2 = y2 + 1;
        z2 = z2 + 1;
    }

    int x2n = 0;
    int y2n = 0;
    int z2n = 0;
    while (x2n < 20) {
        x2n = x2n + 1;
        if (x2n > 150) {
            continue;
        }
        y2n = y2n + 1;
        z2n = z2n + 1;
    }

    int x3 = 0;
    int y3 = 0;
    while (x3 < 20) {
        x3 = x3 + 1;
        y3 = y3 + 1;
        if (x3 > 15) {
            continue;
        }
    }

    int x3n = 0;
    int y3n = 0;
    while (x3n < 20) {
        x3n = x3n + 1;
        y3n = y3n + 1;
        if (x3n > 150) {
            continue;
        }
    }

    printf("20 == %d (y1n)\n", y1n);
    printf("20 == %d (z1n)\n", z1n);
    printf("20 == %d (x2)\n", x2);
    printf("15 == %d (y2)\n", y2);
    printf("15 == %d (z2)\n", z2);
    printf("20 == %d (x2n)\n", x2n);
    printf("20 == %d (y2n)\n", y2n);
    printf("20 == %d (z2n)\n", z2n);
    printf("20 == %d (x3)\n", x3);
    printf("20 == %d (y3)\n", y3);
    printf("20 == %d (x3n)\n", x3n);
    printf("20 == %d (y3n)\n", y3n);
}

void test_for()
{
    int y1 = 0;
    int z1 = 0;
    for (int i = 0; i < 20; i = i + 1) {
        if (i > 15) {
            continue;
        }
        y1 = y1 + 1;
        z1 = z1 + 1;
    }

    int y1n = 0;
    int z1n = 0;
    for (int i = 0; i < 20; i = i + 1) {
        if (i > 150) {
            continue;
        }
        y1n = y1n + 1;
        z1n = z1n + 1;
    }

    int x2 = 0;
    int y2 = 0;
    int z2 = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x2 = x2 + 1;
        if (x2 > 15) {
            continue;
        }
        y2 = y2 + 1;
        z2 = z2 + 1;
    }

    int x2n = 0;
    int y2n = 0;
    int z2n = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x2n = x2n + 1;
        if (x2n > 150) {
            continue;
        }
        y2n = y2n + 1;
        z2n = z2n + 1;
    }

    int x3 = 0;
    int y3 = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x3 = x3 + 1;
        y3 = y3 + 1;
        if (x3 > 15) {
            continue;
        }
    }

    int x3n = 0;
    int y3n = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x3n = x3n + 1;
        y3n = y3n + 1;
        if (x3n > 150) {
            continue;
        }
    }

    printf("16 == %d (y1)\n", y1);
    printf("16 == %d (z1)\n", z1);
    printf("20 == %d (y1n)\n", y1n);
    printf("20 == %d (z1n)\n", z1n);
    printf("20 == %d (x2)\n", x2);
    printf("15 == %d (y2)\n", y2);
    printf("15 == %d (z2)\n", z2);
    printf("20 == %d (x2n)\n", x2n);
    printf("20 == %d (y2n)\n", y2n);
    printf("20 == %d (z2n)\n", z2n);
    printf("20 == %d (x3)\n", x3);
    printf("20 == %d (y3)\n", y3);
    printf("20 == %d (x3n)\n", x3n);
    printf("20 == %d (y3n)\n", y3n);
}


void test_do()
{
    int y1n = 0;
    int z1n = 0;
    do {
        if (y1n > 150) {
            continue;
        }
        y1n = y1n + 1;
        z1n = z1n + 1;
    } while (y1n < 20);

    int x2 = 0;
    int y2 = 0;
    int z2 = 0;
    do {
        x2 = x2 + 1;
        if (x2 > 15) {
            continue;
        }
        y2 = y2 + 1;
        z2 = z2 + 1;
    } while (x2 < 20);

    int x2n = 0;
    int y2n = 0;
    int z2n = 0;
    do {
        x2n = x2n + 1;
        if (x2n > 150) {
            continue;
        }
        y2n = y2n + 1;
        z2n = z2n + 1;
    } while (x2n < 20);

    int x3 = 0;
    int y3 = 0;
    do {
        x3 = x3 + 1;
        y3 = y3 + 1;
        if (x3 > 15) {
            continue;
        }
    } while (x3 < 20);

    int x3n = 0;
    int y3n = 0;
    do {
        x3n = x3n + 1;
        y3n = y3n + 1;
        if (x3n > 150) {
            continue;
        }
    } while (x3n < 20);

    printf("20 == %d (y1n)\n", y1n);
    printf("20 == %d (z1n)\n", z1n);
    printf("20 == %d (x2)\n", x2);
    printf("15 == %d (y2)\n", y2);
    printf("15 == %d (z2)\n", z2);
    printf("20 == %d (x2n)\n", x2n);
    printf("20 == %d (y2n)\n", y2n);
    printf("20 == %d (z2n)\n", z2n);
    printf("20 == %d (x3)\n", x3);
    printf("20 == %d (y3)\n", y3);
    printf("20 == %d (x3n)\n", x3n);
    printf("20 == %d (y3n)\n", y3n);
}

void test_while_while()
{
    int x0n = 0;
    int y0n = 0;
    int y1n = 0;
    int z1n = 0;
    while (x0n < 25) {
        while (y1n < 20) {
            if (y1n > 150) {
                continue;
            }
            y0n = y0n + 1;
            y1n = y1n + 1;
            z1n = z1n + 1;
        }
        x0n = x0n + 1;
        y0n = y0n + 1;
    }

    int x02 = 0;
    int z02 = 0;
    int x2 = 0;
    int y2 = 0;
    while (x02 < 20) {
        x02 = x02 + 1;
        while (x2 < 20) {
            x2 = x2 + 1;
            if (x2 > 15) {
                continue;
            }
            y2 = y2 + 1;
        }
        y2 = y2 + 1;
        z02 = z02 + 1;
    }

    int x02n = 0;
    int z02n = 0;
    int x2n = 0;
    int y2n = 0;
    while (x02n < 20) {
        x02n = x02n + 1;
        while (x2n < 20) {
            x2n = x2n + 1;
            if (x2n > 150) {
                continue;
            }
            y2n = y2n + 1;
        }
        y2n = y2n + 1;
        z02n = z02n + 1;
    }

    int x03 = 0;
    int x3 = 0;
    int y3 = 0;
    while (x03 < 20) {
        x03 = x03 + 1;
        while (x3 < 20) {
            x3 = x3 + 1;
            x03 = x03 + 1;
            y3 = y3 + 1;
            if (x3 > 15) {
                continue;
            }
        }
    }

    int x03n = 0;
    int x3n = 0;
    int y3n = 0;
    while (x03n < 20) {
        x03n = x03n + 1;
        x3n = x3n + 1;
        while (x3n < 20) {
            x3n = x3n + 1;
            x03n = x03n + 1;
            y3n = y3n + 1;
            if (x3n > 150) {
                continue;
            }
        }
    }

    printf("25 == %d (x0n)\n", x0n);
    printf("45 == %d (y0n)\n", y0n);
    printf("20 == %d (z1n)\n", y1n);
    printf("20 == %d (z1n)\n", z1n);
    printf("20 == %d (x02)\n", x02);
    printf("20 == %d (z02)\n", z02);
    printf("20 == %d (x2)\n", x2);
    printf("35 == %d (y2)\n", y2);
    printf("20  == %d (x02n)\n", x02n);
    printf("20  == %d (z02n)\n", z02n);
    printf("20 == %d (x2n)\n", x2n);
    printf("40 == %d (y2n)\n", y2n);
    printf("21 == %d (x03)\n", x03);
    printf("20 == %d (x3)\n", x3);
    printf("20 == %d (y3)\n", y3);
    printf("20  == %d (x03n)\n", x03n);
    printf("20 == %d (x3n)\n", x3n);
    printf("19 == %d (y3n)\n", y3n);
}

void test_for_for()
{
    int y0 = 0;
    int y1 = 0;
    for (int i = 0; i < 17; i = i + 1) {
        for (int j = 0; j < 14; j = j + 1) {
            if (j > 11) {
                continue;
            }
            y0 = y0 + 1;
            y1 = y1 + 1;
        }
        y0 = y0 + 1;
    }

    int y0n = 0;
    int y1n = 0;
    for (int i = 0; i < 17; i = i + 1) {
        for (int j = 0; j < 14; j = j + 1) {
            if (j > 19) {
                continue;
            }
            y0n = y0n + 1;
            y1n = y1n + 1;
        }
        y0n = y0n + 1;
    }

    int x02 = 0;
    int z02 = 0;
    int x2 = 0;
    int y2 = 0;
    int z2 = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x02 = x02 + 1;
        for (int j = 0; j < 20; j = j + 1) {
            x2 = x2 + 1;
            if (j > 15) {
                continue;
            }
            y2 = y2 + 1;
            z2 = z2 + 1;
        }
        y2 = y2 + 1;
        z02 = z02 + 1;
    }

    int x02n = 0;
    int z02n = 0;
    int x2n = 0;
    int y2n = 0;
    int z2n = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x02n = x02n + 1;
        for (int j = 0; j < 20; j = j + 1) {
            x2n = x2n + 1;
            if (j > 150) {
                continue;
            }
            y2n = y2n + 1;
            z2n = z2n + 1;
        }
        y2n = y2n + 1;
        z02n = z02n + 1;
    }

    int x03 = 0;
    int x3 = 0;
    int y3 = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x03 = x03 + 1;
        for (int j = 0; j < 20; j = j + 1) {
            x3 = x3 + 1;
            x03 = x03 + 1;
            y3 = y3 + 1;
            if (j > 15) {
                continue;
            }
        }
    }

    int x03n = 0;
    int x3n = 0;
    int y3n = 0;
    for (int i = 0; i < 20; i = i + 1) {
        x03n = x03n + 1;
        for (int j = 0; j < 20; j = j + 1) {
            x3n = x3n + 1;
            x03n = x03n + 1;
            y3n = y3n + 1;
            if (j > 150) {
                continue;
            }
        }
    }

    printf("221 == %d (y0)\n", y0);
    printf("204 == %d (y1)\n", y1);
    printf("255  == %d (y0n)\n", y0n);
    printf("238  == %d (y1n)\n", y1n);
    printf("20 == %d (x02)\n", x02);
    printf("20 == %d (z02)\n", z02);
    printf("400 == %d (x2)\n", x2);
    printf("340 == %d (y2)\n", y2);
    printf("320 == %d (z2)\n", z2);
    printf("20  == %d (x02n)\n", x02n);
    printf("20  == %d (z02n)\n", z02n);
    printf("400  == %d (x2n)\n", x2n);
    printf("420  == %d (y2n)\n", y2n);
    printf("400  == %d (z2n)\n", z2n);
    printf("420  == %d (x03)\n", x03);
    printf("400 == %d (x3)\n", x3);
    printf("400 == %d (y3)\n", y3);
    printf("420  == %d (x03n)\n", x03n);
    printf("400  == %d (x3n)\n", x3n);
    printf("400  == %d (y3n)\n", y3n);
}

void test_do_do()
{
    int x0n = 0;
    int y0n = 0;
    int y1n = 0;
    int z1n = 0;
    do {
         do {
            if (y1n > 150) {
                continue;
            }
            y0n = y0n + 1;
            y1n = y1n + 1;
            z1n = z1n + 1;
        } while (y1n < 20);
        x0n = x0n + 1;
        y0n = y0n + 1;
    } while (x0n < 25);

    int x02 = 0;
    int z02 = 0;
    int x2 = 0;
    int y2 = 0;
    do {
        x02 = x02 + 1;
        do {
            x2 = x2 + 1;
            if (x2 > 15) {
                continue;
            }
            y2 = y2 + 1;
        } while (x2 < 20);
        y2 = y2 + 1;
        z02 = z02 + 1;
    } while (x02 < 20);

    int x02n = 0;
    int z02n = 0;
    int x2n = 0;
    int y2n = 0;
    do {
        x02n = x02n + 1;
        do {
            x2n = x2n + 1;
            if (x2n > 150) {
                continue;
            }
            y2n = y2n + 1;
        } while (x2n < 20);
        y2n = y2n + 1;
        z02n = z02n + 1;
    } while (x02n < 20);

    int x03 = 0;
    int x3 = 0;
    int y3 = 0;
    do {
        x03 = x03 + 1;
        do {
            x3 = x3 + 1;
            x03 = x03 + 1;
            y3 = y3 + 1;
            if (x3 > 15) {
                continue;
            }
        } while (x3 < 20);
    } while (x03 < 20);

    int x03n = 0;
    int x3n = 0;
    int y3n = 0;
    do {
        x03n = x03n + 1;
        x3n = x3n + 1;
        do {
            x3n = x3n + 1;
            x03n = x03n + 1;
            y3n = y3n + 1;
            if (x3n > 150) {
                continue;
            }
        } while (x3n < 20);
    } while (x03n < 20);

    printf("25 == %d (x0n)\n", x0n);
    printf("69 == %d (y0n)\n", y0n);
    printf("44 == %d (z1n)\n", y1n);
    printf("44 == %d (z1n)\n", z1n);
    printf("20 == %d (x02)\n", x02);
    printf("20 == %d (z02)\n", z02);
    printf("39 == %d (x2)\n", x2);
    printf("35 == %d (y2)\n", y2);
    printf("20  == %d (x02n)\n", x02n);
    printf("20  == %d (z02n)\n", z02n);
    printf("39 == %d (x2n)\n", x2n);
    printf("59 == %d (y2n)\n", y2n);
    printf("21 == %d (x03)\n", x03);
    printf("20 == %d (x3)\n", x3);
    printf("20 == %d (y3)\n", y3);
    printf("20  == %d (x03n)\n", x03n);
    printf("20 == %d (x3n)\n", x3n);
    printf("19 == %d (y3n)\n", y3n);
}

int main() {

    test_while();
    test_for();
    test_do();
    test_while_while();
    test_for_for();
    test_do_do();

    return 0;
}
