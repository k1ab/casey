# Hook tests

Here we make sure that "hooked" functions behave as expected.

## Run in Pytest

In order to run all tests in this file run:

```example
pytest --codeblocks tests/semantics/hook.md
```

## Compile

In order to enable hooked functions they need to be compiled with extra flags:

```bash
kompile --hook-namespaces IOLIB -ccopt semantics/hook/iolib.cpp semantics/top/main-cmm.md
```

## printf

Our test snippet:

```C
--8<-- "tests/semantics/example/print/print.c"
```

```bash
krun tests/semantics/example/print/print.c --output none
```

## Cleanup

Now we can remove temporary folders built by the [Compile](#compile) step.

```bash
rm -r main-cmm-kompiled
```
