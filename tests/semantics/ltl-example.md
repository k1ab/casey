# LTL module usage example

Here we provide LTL module usage example with simple semantics and tests.

## Requirements

```{.k .ltl-test. .requirements}
requires "../../semantics/ltl/ltl.md"
```

## States and labels

[LTL module](../../semantics/ltl/ltl.md) expects user module to provide states and labels.
We define a simple grammar that describes a sequence of states with specific labels.
For example `s1<l1,l2>;s2<l1>;s3<l3>` means that there are 3 states:
`s1`, `s2`, and  `s3`.
State `s1` has labels `l1` and `l2`.
State `s2` has label `l1`.
State `s3` has label `l3`.

## Syntax

First we import LTL syntax and built-in modules:

```{.k .ltl-example .syntax}
module LTL-EXAMPLE-SYNTAX
```

```{.k .ltl-example .syntax}
  imports LTL-SYNTAX
  imports DOMAINS
```

We define `StateId` and `StateLabel` as built-in `Id`.
Built-in `Id`s are similar to the standard C identifiers.

```{.k .ltl-example .syntax}
  syntax StateId ::= Id
  syntax StateLabel ::= Id
```

Based on our grammar `Labels` sort is a comma separated list of labels:

```{.k .ltl-example .syntax}
  syntax Labels ::= List{StateLabel, ","}
```

State sort is a `StateId` with `Labels`.

```{.k .ltl-example .syntax}
    syntax State ::= StateId "<" Labels ">"
```

We can define a sequence of states as

```{.k .ltl-example .syntax}
  syntax States ::= List{State, ";"}
```

```{.k .ltl-example .syntax}
endmodule
```

## Configuration

```{.k .ltl-example .config}
module CONFIG
```

```{.k .ltl-test .config}
  imports LTL-EXAMPLE-SYNTAX
  imports LTL-CONFIG
```

```{.k .ltl-example .config}
  configuration
    <k> $PGM:States </k>
    <states> .List </states>
    <ltl/>

```

```{.k .ltl-example .config}
endmodule
```

## Semantics

```{.k .ltl-example .semantics}
module LTL-EXAMPLE
```

Here we import `LTL` semantics and our test syntax:

```{.k .ltl-test .semantics}
  imports LTL
  imports CONFIG
  imports LTL-EXAMPLE-SYNTAX
```

Process each `State` on by one.

```{.k .ltl-example .semantics}
  rule <k> St:State; Sts => St ~> Sts ... </k>
```

Collect all the state labels.

```{.k .ltl-example .semantics}
  rule <k> S < L, Ls > => S < Ls > ... </k>
    <ltl>
      ...
      <labels> _:Set (.Set => SetItem(L)) </labels>
    </ltl>
```

When we have our current state ready and all the labels for the state are collected, we can apply `LTLEval` provided by the `LTL` module.
The following rule takes the current state and puts it in the processed states list.
It also takes the current formula and plugs it into `LTLEval` with all the collected labels.

```{.k .ltl-example .semantics}
  rule <k> St < .Labels > => . ... </k>
    <states> _:List (.List => ListItem(St)) </states>
    <ltl>
      ...
      <formula> F => LTLEval(F, Ls) </formula>
      <labels> Ls => .Set </labels>
    </ltl>
```

When we have no more states left we call `LTLFinalize`.

```{.k .ltl-example .semantics}
  rule <k> .States => . ... </k>
  <ltl>
    ...
    <formula> F => LTLFinalize(F) </formula>
    ...
  </ltl>
```

Here we don't need to do anything with `LTLEval` or `LTLFinalize`.
`LTL` module has all necessary rules to process it correctly.

```{.k .ltl-example .semantics}
endmodule
```

## Tests

Base on our simple example grammar we can test our `LTL` module.

### Compile

```bash
kompile tests/semantics/ltl-example.md
```

### Positive

#### Always l2

Label `l2` is present in each state so given ltl formula `[] l2`:

```bash
krun -cPGM="s1<l1,l2,l3>;s2<l2>" -cLTL="[] l2"
```

We expect that the `<formula>` cell will be `True`.
The whole output should be:

<!--pytest-codeblocks:expected-output-->
```output
<generatedTop>
  <k>
    .
  </k>
  <states>
    ListItem ( s1 )
    ListItem ( s2 )
  </states>
  <ltl>
    <formula>
      True
    </formula>
    <labels>
      .Set
    </labels>
  </ltl>
<generatedCounter>
  0
</generatedCounter>
```

#### Eventually l3

Here we check that eventually we get `l3`.

```bash
krun -cPGM="s1<l1,l2>;s2<l2>;s3<l3>" -cLTL="<> l3"
```

We expect that the `<formula>` cell will contain `True`.
The whole output should be:

<!--pytest-codeblocks:expected-output-->
```output
<generatedTop>
  <k>
    .
  </k>
  <states>
    ListItem ( s1 )
    ListItem ( s2 )
    ListItem ( s3 )
  </states>
  <ltl>
    <formula>
      True
    </formula>
    <labels>
      .Set
    </labels>
  </ltl>
<generatedCounter>
  0
</generatedCounter>
```

### Negative

### Cleanup

```bash
rm -r ltl-example-kompiled
```
