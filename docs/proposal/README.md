# About

This section contains a list of proposals for new features and improvements.
Before it is clear how to implement certain features, we often need to do exploration to define boundaries of the task.
Proposal system is here to capture that work.

Not every feature needs to go through the proposal system.
Here are some reasons to create a proposal:

  * There is an existing feature but there might be a better way to do it.
  * The scope of the feature is not clear.
  * It is unclear how to implement the feature or there might be multiple ways to do it.

All the proposals live in the following sections:

<!--nav-->
* [Proposed](proposed/)
* [Accepted](accepted/)
* [Declined](declined/)

There are two stages that affect proposals:

## Initial research

The goal here is to describe what needs to be implemented.
It also should contain exploration of different implementation approaches.

```mermaid
flowchart TD
    CreateIssue[Start/Create a related issue in Gitlab] --> CreateMerge[Create a merge request]
    CreateMerge --> ProposalFiles[Add proposal files to the <b>Proposed</b> folder]
    ProposalFiles --> Evaluation{Proposal evaluation}
    Evaluation --> Declined
    Evaluation --> Accepted
    Evaluation --> Clarification[Needs clarification]
    Clarification --> Evaluation
    Declined --> UpdateDeclined[Move proposal files to <b>Declined</b> folder]
    UpdateDeclined --> DeclinedIssue[Update the issue with the reason why proposal was declined]
    DeclinedIssue --> Merge
    Accepted --> ImplementationIssue[Add new Gitlab issue to implement the feature with a link to the proposal]
    ImplementationIssue --> Merge
```

## Implementation stage

The goal here is to implement the feature.

```mermaid
flowchart TD
    ImplementationIssue[Start working on the issue created during initial research] --> CreateMerge[Create a merge request]
    CreateMerge --> Implement
    Implement --> UpdateProposed[Move proposal files from <b>Proposed</b> to <b>Accepted</b> folder]
    UpdateProposed --> UpdateAccepted[Add links to current implementation to the proposal files]
    UpdateAccepted--> UpdateDocs[Update documentation]
```
