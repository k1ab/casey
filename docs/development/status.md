# Development status

This is an overview page with our plans at the top and the development history going down.
Each section is an epic with links to relevant milestones.

## V1

The goal of this epic is to provide initial version of the software.

### Finalize v1 syntax

Review and finalize [syntax that needs to be supported for v1](https://gitlab.com/k1ab/casey/-/milestones/17#tab-issues).

### Internal representation

We discovered several [internal representation](https://gitlab.com/k1ab/casey/-/milestones/18#tab-issues) improvements
that need to be done before we move forward.

### State processing

The goal of [state processing](https://gitlab.com/k1ab/casey/-/milestones/19#tab-issues) milestone
is to provide a way for the user to add extra information to new states.
For example users should be able to define custom labels and run LTL formula validation for each state.

### Manager v1
This epic is split into two milestones [design](https://gitlab.com/k1ab/casey/-/milestones/20#tab-issues)
and [implementation](https://gitlab.com/k1ab/casey/-/milestones/21#tab-issues).
It includes a wrapper around generated interpreter and an API server for interacting with the generated graph of states.

## Post-MVP

The goal of this epic was to address issues faced after MVP so we can move forward.

### Maintainability

A lot of effort went into improving [maintainability](https://gitlab.com/k1ab/casey/-/milestones/22#tab-issues).
After the initial MVP we realized that we have to use literate programming style for keeping all the rules organized.

### Testability

The other area that we improved was [testability](https://gitlab.com/k1ab/casey/-/milestones/23#tab-issues).
This worked well with our new documentation style because we have testable examples of supported C features.

### Bug fixes

[We fixed several bugs](https://gitlab.com/k1ab/casey/-/milestones/25#tab-issues) after improving the project testability.

### Initial graph manager research

After MVP we realized that we need a manager for generating and managing the state graph.
We dedicated some time to [explore](https://gitlab.com/k1ab/casey/-/milestones/24#tab-issues) how such manager should look like.

## MVP

Complete and planned features represented in a form of a Tech Tree.
Implemented features are marked by a checkmark &#10004;.

### General overview

```mermaid
flowchart TD
    POC["Proof Of Concept #10004;"]-->Kompile["pre compile for faster seq check #10004;"]
    POC-->Cmm("C-- semantics")
    Cmm-->For["For loop"]
    Cmm-->Hex
    Cmm-->Octal
    POC-->Docs("Docs")
    Docs-->DocsDeploy["Auto deploy #10004;"]
    Docs-->DocsStructure["Design docs structure"]
    Docs-->Articles("Articles")
    DocsDeploy-->mkdocs["mkdocs #10004;"]
    DocsDeploy-->readthedocs["readthedocs #10004;"]
    Kompile-->DC("Distributed Compute")
    DC-->DP["Design Protocol #10004;"]
    DP-->Server("Server")
    DP-->Worker("Worker")
```

### Server

```mermaid
flowchart TD
    Server("Server")-->API["API #10004;"]
    Server-->GP["Graph representation"]
```

### Worker

```mermaid
flowchart TD
    Worker("Worker")-->Protocol["State Protocol #10004;"]
    Worker-->NodeGen("Node Generation")
    NodeGen-->SeqList["Run interpreter with sequence lists #10004;"]
    Protocol-->Cache["Cache Computed Nodes"]
    Cache-->SendNodes["Send only requested nodes"]
```

### Chart legend

```mermaid
flowchart TD
    A["Concrete task"]
```

```mermaid
flowchart TD
    A["Complete task #10004;"]
```

```mermaid
flowchart TD
    A("Abstract group of tasks")
```

```mermaid
flowchart TD
    A --> |B depends on A| B
```
