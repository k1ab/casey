# About

This project is in early development stage so not everything is implemented.
Check out [development status](status.md) page for overview of the progress.

## Run locally

Clone the repository:

```bash
git clone https://gitlab.com/k1ab/casey.git
cd casey
```

Start dev container:

```bash
docker-compose up -d
docker exec -it casey_dev /bin/bash
```

Inside the container, all K framework and Casey tools will be available.

## Development navigation

<!--nav-->
* [About](README.md)
* [Development status](status.md)
* [Proposal](../proposal/)
* [External references](external-references.md)
* [Writing docs](documentation.md)
