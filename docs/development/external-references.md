# External References

## C standard

Getting an official standard costs money but there is a reference to
[C - Project status and milestones](https://www.open-std.org/jtc1/sc22/wg14/www/projects)
which has a link to [C99 committee draft](https://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf).
That document is official enough for our purpose. We will use it as "the standard" throughout this documentation.

## K framework and related projects

* [K Framework repository](https://github.com/runtimeverification/k)
* [K Framework main page](https://kframework.org/)
* [Semantics of C in K](https://github.com/kframework/c-semantics)

## Matching logic

* [Matching logic](http://www.matching-logic.org/)
