# About

## Local requirements

From project root:

```bash
pip install -r docs/requirements.txt
```

## Running doc server

```bash
mkdocs serve
```

Docs will be served [locally](http://127.0.0.1:8000/).
