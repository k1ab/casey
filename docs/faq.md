# Frequently Asked Questions

## How does Casey compare to TLA+/TLC?

### Input language

Casey supports a subset of C as an input so the model specification and compiled code is the same thing.
The subset of C can be compiled using popular compilers like `clang` or `gcc`.

TLA+ has its own language, PlusCal, to define models.
This leads to divergence between actual code that goes into prod and model specification.
There are tools that generate C from PlusCal.
This is often inconvenient because generated code can't be changed or adopted for other considerations.
There are tools that generate PlusCal from C.
This is a non trivial conversion and raises the question of validating the conversion step.
How do we make sure that the resulting PlusCal semantics are the same as the source C semantics?
In Casey, we deliberately [address the problem](#how-do-we-make-sure-that-clang-gcc-and-casey-agree-on-c-language-features)
and make sure that the semantics of supported C-- features match exactly how `clang` and `gcc` interpret those features.

### Tool purpose

Casey specializes in automatically validating code properties of specific software.
It is especially useful for verifying multi-threaded synchronization mechanisms.
Meanwhile, TLA+ is good for verifying behavioral properties of abstract general purpose systems.

Due to the nature of K framework, defining states and working with them in Casey is easy.
In order to do the same thing in TLA+, one has to change the TLC source code or build plugins.

When building a model, Casey can account for different execution environments where code is executed.
For example, we can verify the same source code with weak or strong memory models.

### Taking compilation into account

Casey is built in such a way that input language can be replaced without changing other components.
For example, Casey can make sure that the same properties hold for source code written in C-- and x86 assembly language.
Since only input language was changed, we can verify that important properties hold after the compilation step even if we don't use certified compilers.

## How do we make sure that Clang, GCC, and Casey agree on C language features?

We generate a lot of random C code.
We compile it with `clang` and `gcc`.
We run the resulting binaries and compare results with the result from Casey C-- interpreter.
If there are differences between `clang` and `gcc`, we do not support that language feature.
For every feature that we support in our subset of C, all results must match.

## How Casey can improve my C code if it doesn't support some C features?

Casey enables programmers to define requirements as code and automatically validates that the code has some properties.
Casey is not built to find coding errors. The goal of the project is to keep code free from logical errors.
A [good example](../tests/environment/peterson.md) of how Casey can be used is shown based synchronizing multiple threads using Peterson's algorithm.
