FROM k1ab/casey-env:v0.2.2

WORKDIR /home/casey/app

RUN mkdir c-- casey ltl

COPY c-- ./c--
COPY casey ./casey
COPY ltl ./ltl
COPY cli ./

USER root
RUN chown casey:casey casey/tools/bin/
USER casey
