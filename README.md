# Casey

A tool to automatically validate code properties.

Official [project documentation](https://casey.readthedocs.io/en/latest/).

## Goal

Improve software quality beyond testing.

## How

* Provide a way to explicitly state behavioral software properties.
* Automatically validate that the stated properties hold.

## Why

Often when implementing protocols, it is unclear what might break and therefore what should be covered by tests.
In concurrent code, it is not practical to check all possible combinations of threads execution.
Tests often don't have a systematic way to consider an environment where the software is executed.
Usually tests implicitly verify software properties.
Some properties are easy to express as tests.
For example, we expect that square function will return 4 when given an input of 2.
More interesting properties are hard to express in tests.
For example, if we implement communication between a server and a client, we expect that there will be no situation when both the server and the client are stuck waiting for each other’s responses.
In those cases it is more natural to express what we expect from a software as a software property.

## Approach

We use model checking approach where a real system is modeled by a finite automata
and required behavioral properties are expressed precisely by temporal logic formulas.

In order to automatically build a model, we need a way to express a program, its properties,
and a critical part of the environment where the program runs.
[Semantics](semantics/) section uses K framework to define a [subset of C language](semantics/c--/),
[environment](semantics/environment/) where the program runs,
and properties expressed as [linear temporal logic formulas](semantics/ltl/).

Using K framework tools we generate an interpreter that takes a source code file
and executes the code in the given environment for a predefined number of steps.
The result is a program state that can be fed into the same interpreter to generate next program state.
In each state we validate that the properties expressed as ltl formulas still hold.

[Manager](manager/) section uses the interpreter to generate a graph of all possible program states.
By analyzing the graph we can prove that the properties hold for all possible program paths in our model.
Otherwise we can produce an execution path to a state where a property is no longer true.

```mermaid
classDiagram
    Semantics ..> K Framework
    K Framework ..|> Interpreter
    Interpreter ..> Manager
    Manager *-- Server
    Manager *-- Worker
    GraphDB ..> Server
    Server ..|> Sate Graph

    class Semantics {
        C--
        Environment
        LTL
    }
```

## Development

Checkout [developers section](docs/development/).

## Development status

There is a [development status](docs/development/status.md) overview page.

## Contributing

Create a merge request against main branch. In the merge request, explain:

* What it adds to the project
* Why it is necessary
* List related issues

## Support

Support is done through Gitlab [issues](https://gitlab.com/k1ab/casey/-/issues)

## License

MIT License

## Main navigation

<!--nav-->
* [Home](README.md)
* [FAQ](docs/faq.md)
* [Semantics](semantics/)
* [Manager](manager/)
* [Development](docs/development/)
